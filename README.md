# dynamic-grid-vaadin-spring-boot-starter
This is just a POC!  
The goal is to generate Vaadin views based on simple entities.

- [grid-annotation](https://gitlab.com/overcoded.io/grid-annotation)
- [grid-processor](https://gitlab.com/overcoded.io/grid-processor)
- [dynamic-repository](https://gitlab.com/overcoded.io/dynamic-repository)

## How to use

Add `@EnableVaadin({"io.overcoded.vaadin", "your.package.name"})` to your application.
