package io.overcoded.grid;

import lombok.Data;

import java.util.List;

@Data
public class ColumnConfigurationProperties {
    private boolean columnReorderingAllowed = true;
    private boolean autoWidthEnabled = true;
    private boolean sortable = true;
    private boolean resizable = true;
    private int pageSize = 20;
    private boolean badgesForBooleanEnabled = true;
    private boolean falsyValuesEnabled = true;
    private boolean multiSortEnabled = true;
    private boolean footerEnabled = false;
    private List<String> enabledFields = List.of("enabled", "active");
    private String idColumnName = "id";
}
