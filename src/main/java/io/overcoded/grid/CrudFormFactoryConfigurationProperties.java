package io.overcoded.grid;

import lombok.Data;

@Data
public class CrudFormFactoryConfigurationProperties {
    private String idColumnName = "id";
    private boolean disableIdColumn = true;
}
