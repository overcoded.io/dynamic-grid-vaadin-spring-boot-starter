package io.overcoded.grid;

import lombok.Data;

@Data
public class DialogProperties {
    private boolean withPadding = false;
    private boolean fullSized = true;
    private boolean modal = true;
    private boolean resizable = true;
    private boolean draggable = true;
    private boolean closeOnEscape = false;
    private boolean closeOnOutsideClick = false;
}
