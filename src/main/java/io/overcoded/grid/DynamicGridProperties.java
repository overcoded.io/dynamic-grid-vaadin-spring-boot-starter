package io.overcoded.grid;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

@Data
@Getter(onMethod = @__({@Bean}))
@ConfigurationProperties(prefix = "dynamic-grid")
public class DynamicGridProperties {
    private DialogProperties dialog = new DialogProperties();
    private GridCrudProperties gridCrud = new GridCrudProperties();
    private ColumnConfigurationProperties column = new ColumnConfigurationProperties();
    private GridSecurityConfigurationProperties gridSecurity = new GridSecurityConfigurationProperties();
    private CrudFormFactoryConfigurationProperties crudFormFactory = new CrudFormFactoryConfigurationProperties();
}
