package io.overcoded.grid;

import io.overcoded.vaadin.layout.ReconfiguredHorizontalSplitCrudLayout;
import lombok.RequiredArgsConstructor;
import org.vaadin.crudui.layout.CrudLayout;
import org.vaadin.crudui.layout.impl.VerticalCrudLayout;
import org.vaadin.crudui.layout.impl.VerticalSplitCrudLayout;
import org.vaadin.crudui.layout.impl.WindowBasedCrudLayout;

import java.util.function.Supplier;

@RequiredArgsConstructor
public enum GridCrudLayout {
    WINDOW_BASED(WindowBasedCrudLayout::new),
    VERTICAL(VerticalCrudLayout::new),
    HORIZONTAL_SPLIT(ReconfiguredHorizontalSplitCrudLayout::new),
    VERTICAL_SPLIT(VerticalSplitCrudLayout::new);

    private final Supplier<CrudLayout> factory;

    public CrudLayout create() {
        return factory.get();
    }
}
