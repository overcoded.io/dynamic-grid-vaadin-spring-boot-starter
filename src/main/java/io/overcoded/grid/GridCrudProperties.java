package io.overcoded.grid;

import io.overcoded.grid.annotation.FieldProviderType;
import lombok.Data;

import java.util.Set;

@Data
public class GridCrudProperties {
    private boolean singleColumnOnly = true;
    private GridCrudLayout layout = GridCrudLayout.HORIZONTAL_SPLIT;
    private GridCrudLayout withoutFormLayout = GridCrudLayout.WINDOW_BASED;
    private GridCrudLayout dialogLayout = GridCrudLayout.HORIZONTAL_SPLIT;
    private Set<FieldProviderType> singleColumnFieldProviderTypes = Set.of(FieldProviderType.RICH_TEXT, FieldProviderType.SOURCE_CODE, FieldProviderType.TEXT_AREA);
}
