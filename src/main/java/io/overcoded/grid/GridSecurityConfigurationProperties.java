package io.overcoded.grid;

import io.overcoded.grid.security.ActionType;
import io.overcoded.grid.security.GridRole;
import io.overcoded.grid.security.GridUser;
import io.overcoded.grid.security.GridUserActivity;
import lombok.Data;
import org.javers.core.diff.ListCompareAlgorithm;

import java.time.format.DateTimeFormatter;
import java.util.Map;

@Data
public class GridSecurityConfigurationProperties {
    private Class<? extends GridUser> userEntity;
    private Class<? extends GridRole> roleEntity;
    private Class<? extends GridUserActivity> userActivityEntity;
    private int activityHistorySize = 50;
    private boolean activityTrackingEnabled = true;
    private boolean differenceTrackingEnabled = true;
    private boolean activityTrackingOnBackgroundThread = true;
    private ListCompareAlgorithm listCompareAlgorithm = ListCompareAlgorithm.SIMPLE;
    private DateTimeFormatter activityTimestampFormatter = DateTimeFormatter.ISO_DATE_TIME;

    private Map<ActionType, String> actionMessageFormat = Map.of(
            ActionType.CREATE, "%1s: %2s created a new %3s (#%4d)",
            ActionType.MODIFY, "%1s: %2s modified an existing %3s (#%4d)",
            ActionType.DELETE, "%1s: %2s deleted an existing %3s (#%4d)"
    );
    private String unknownActionMessageFormat = "%1s: %2s made an unknown action on %3s (#%4d)";
    private String activityTrackerDisabledTitle = "Activity tracker is disabled";
    private String activityTrackerDisabledMessage = "If you want to track user activity, you can enable it by setting dynamic-grid.activity-tracking-enabled=true.";
    private String differenceTrackerDisabledTitle = "Difference tracker is disabled";
    private String differenceTrackerDisabledMessage = "If you want to track user activity, you can enable it by setting dynamic-grid.difference-tracking-enabled=true.";
    private String activityTitle = "Past activities";
    private String changePasswordTitle = "Change password";

}
