package io.overcoded.grid.processor;

import io.overcoded.grid.DynamicGridProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Role;

@RequiredArgsConstructor
@Configuration(proxyBeanMethods = false)
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
@AutoConfigureAfter(JacksonAutoConfiguration.class)
@EnableConfigurationProperties(DynamicGridProperties.class)
@ComponentScan({"io.overcoded.grid", "io.overcoded.vaadin"})
@Import({DynamicVaadinSecurityConfiguration.class, DynamicVaadinDependencyConfiguration.class, DynamicVaadinDialogConfiguration.class, DynamicVaadinFieldConfiguration.class, DynamicVaadinGridConfiguration.class, DynamicVaadinLayoutConfiguration.class})
public class DynamicGridVaadinAutoConfiguration {

}
