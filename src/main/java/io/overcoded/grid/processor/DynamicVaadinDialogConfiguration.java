package io.overcoded.grid.processor;

import io.overcoded.grid.processor.dialog.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.List;

@Configuration(proxyBeanMethods = false)
@Import(DynamicVaadinGridConfiguration.class)
public class DynamicVaadinDialogConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public OneToOneDialogTypeEvaluator oneToOneDialogTypeEvaluator() {
        return new OneToOneDialogTypeEvaluator();
    }

    @Bean
    @ConditionalOnMissingBean
    public OneToManyDialogTypeEvaluator oneToManyDialogTypeEvaluator() {
        return new OneToManyDialogTypeEvaluator();
    }

    @Bean
    @ConditionalOnMissingBean
    public ManyToManyDialogTypeEvaluator manyToManyDialogTypeEvaluator() {
        return new ManyToManyDialogTypeEvaluator();
    }

    @Bean
    @ConditionalOnMissingBean
    public ReverseManyToOneDialogTypeEvaluator reverseManyToOneDialogTypeEvaluator() {
        return new ReverseManyToOneDialogTypeEvaluator();
    }

    @Bean
    @ConditionalOnMissingBean
    public DialogTypeDecider dialogTypeDecider(FieldCollector fieldCollector,
                                               GridDialogValidator gridDialogValidator,
                                               List<DialogTypeEvaluator> dialogTypeEvaluators) {
        return new DialogTypeDecider(fieldCollector, gridDialogValidator, dialogTypeEvaluators);
    }


    @Bean
    @ConditionalOnMissingBean
    public DialogInfoFactory dialogInfoFactory(GridInfoDialogFactory gridInfoDialogFactory, DialogTypeDecider dialogTypeDecider) {
        return new DialogInfoFactory(gridInfoDialogFactory, dialogTypeDecider);
    }
}
