package io.overcoded.grid.processor;

import io.overcoded.grid.PickerFactory;
import io.overcoded.grid.processor.column.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.repository.support.Repositories;

import java.util.List;

@Configuration(proxyBeanMethods = false)
@Import({DynamicVaadinLayoutConfiguration.class, DynamicVaadinDependencyConfiguration.class})
public class DynamicVaadinFieldConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public FieldCollector fieldCollector() {
        return new FieldCollector();
    }

    @Bean
    @ConditionalOnMissingBean
    public GridDialogValidator gridDialogValidator() {
        return new GridDialogValidator();
    }

    @Bean
    @ConditionalOnMissingBean
    public DisplayValueExpressionExtractor displayValueExpressionExtractor() {
        return new DisplayValueExpressionExtractor();
    }

    @Bean
    @ConditionalOnMissingBean
    public DatePickerFieldProviderTypeEvaluator datePickerFieldProviderTypeEvaluator() {
        return new DatePickerFieldProviderTypeEvaluator();
    }

    @Bean
    @ConditionalOnMissingBean
    public DateTimePickerFieldProviderTypeEvaluator dateTimePickerFieldProviderTypeEvaluator() {
        return new DateTimePickerFieldProviderTypeEvaluator();
    }

    @Bean
    @ConditionalOnMissingBean
    public EnumFieldProviderTypeEvaluator enumFieldProviderTypeEvaluator() {
        return new EnumFieldProviderTypeEvaluator();
    }

    @Bean
    @ConditionalOnMissingBean
    public JpaRepositoryFieldProviderTypeEvaluator jpaRepositoryFieldProviderTypeEvaluator(Repositories repositories) {
        return new JpaRepositoryFieldProviderTypeEvaluator(repositories);
    }

    @Bean
    @ConditionalOnMissingBean
    public PickerFieldProviderTypeEvaluator pickerFieldProviderTypeEvaluator(List<PickerFactory<?>> pickers) {
        return new PickerFieldProviderTypeEvaluator(pickers);
    }

    @Bean
    @ConditionalOnMissingBean
    public TimePickerFieldProviderTypeEvaluator timePickerFieldProviderTypeEvaluator() {
        return new TimePickerFieldProviderTypeEvaluator();
    }

    @Bean
    @ConditionalOnMissingBean
    public FieldProviderTypeDecider fieldProviderTypeDecider(List<FieldProviderTypeEvaluator> fieldProviderTypeEvaluators) {
        return new FieldProviderTypeDecider(fieldProviderTypeEvaluators);
    }
}
