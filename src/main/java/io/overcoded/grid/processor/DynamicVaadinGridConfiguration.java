package io.overcoded.grid.processor;

import io.overcoded.grid.processor.column.DisplayValueExpressionExtractor;
import io.overcoded.grid.processor.column.FieldProviderTypeDecider;
import io.overcoded.grid.processor.column.JoinFieldFinder;
import io.overcoded.grid.processor.column.NameTransformer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration(proxyBeanMethods = false)
@Import(DynamicVaadinFieldConfiguration.class)
public class DynamicVaadinGridConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public ColumnInfoFactory columnInfoFactory(NameTransformer nameTransformer,
                                               GridDialogValidator gridDialogValidator,
                                               FieldProviderTypeDecider fieldProviderTypeDecider,
                                               DisplayValueExpressionExtractor displayValueExpressionExtractor) {
        return new ColumnInfoFactory(fieldProviderTypeDecider, displayValueExpressionExtractor, gridDialogValidator, nameTransformer);
    }

    @Bean
    @ConditionalOnMissingBean
    public ColumnInfoCollector columnInfoCollector(ColumnInfoFactory columnInfoFactory, FieldCollector fieldCollector) {
        return new ColumnInfoCollector(columnInfoFactory, fieldCollector);
    }

    @Bean
    @ConditionalOnMissingBean
    public JoinFieldFinder joinFieldFinder(FieldCollector fieldCollector, NameTransformer nameTransformer) {
        return new JoinFieldFinder(fieldCollector, nameTransformer);
    }

    @Bean
    @ConditionalOnMissingBean
    public GridMenuEntryFactory gridMenuEntryFactory(JoinFieldFinder joinFieldFinder, NameTransformer nameTransformer) {
        return new GridMenuEntryFactory(joinFieldFinder, nameTransformer);
    }

    @Bean
    @ConditionalOnMissingBean
    public GridMenuEntryCollector gridMenuEntryCollector(FieldCollector fieldCollector,
                                                         GridDialogValidator gridDialogValidator,
                                                         GridMenuEntryFactory gridMenuEntryFactory) {
        return new GridMenuEntryCollector(fieldCollector, gridDialogValidator, gridMenuEntryFactory);
    }

    @Bean
    @ConditionalOnMissingBean
    public GridMenuGroupFactory groupFactory() {
        return new GridMenuGroupFactory();
    }

    @Bean
    @ConditionalOnMissingBean
    public GridMenuGroupCollector gridMenuGroupCollector(GridMenuGroupFactory groupFactory,
                                                         GridMenuEntryCollector gridMenuEntryCollector) {
        return new GridMenuGroupCollector(groupFactory, gridMenuEntryCollector);
    }

    @Bean
    @ConditionalOnMissingBean
    public GridInfoViewFactory gridInfoViewFactory(NameTransformer nameTransformer,
                                                   ColumnInfoCollector columnInfoCollector,
                                                   GridMenuGroupCollector gridMenuGroupCollector) {
        return new GridInfoViewFactory(nameTransformer, columnInfoCollector, gridMenuGroupCollector);
    }

    @Bean
    public GridInfoDialogFactory gridInfoDialogFactory(NameTransformer nameTransformer,
                                                       ColumnInfoCollector columnInfoCollector,
                                                       GridMenuGroupCollector gridMenuGroupCollector) {
        return new GridInfoDialogFactory(nameTransformer, columnInfoCollector, gridMenuGroupCollector);
    }
}
