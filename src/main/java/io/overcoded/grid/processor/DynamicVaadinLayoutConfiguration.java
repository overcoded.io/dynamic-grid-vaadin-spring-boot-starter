package io.overcoded.grid.processor;

import io.overcoded.grid.processor.column.NameTransformer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class DynamicVaadinLayoutConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public ReflectionsFactory reflectionsFactory() {
        return new ReflectionsFactory();
    }

    @Bean
    @ConditionalOnMissingBean
    public NameTransformer nameTransformer() {
        return new NameTransformer();
    }

    @Bean
    @ConditionalOnMissingBean
    public SubmenuEntryCollector submenuEntryCollector() {
        return new SubmenuEntryCollector();
    }

    @Bean
    @ConditionalOnMissingBean
    public MenuEntryFactory menuEntryFactory(NameTransformer nameTransformer) {
        return new MenuEntryFactory(nameTransformer);
    }

    @Bean
    @ConditionalOnMissingBean
    public MenuEntryCollector menuEntryCollector(ReflectionsFactory reflectionsFactory, MenuEntryFactory menuEntryFactory, SubmenuEntryCollector submenuEntryCollector) {
        return new MenuEntryCollector(menuEntryFactory, reflectionsFactory, submenuEntryCollector);
    }

    @Bean
    @ConditionalOnMissingBean
    public MenuGroupFactory menuGroupFactory(MenuEntryCollector menuEntryCollector, NameTransformer nameTransformer) {
        return new MenuGroupFactory(menuEntryCollector, nameTransformer);
    }

    @Bean
    @ConditionalOnMissingBean
    public MenuGroupCollector menuGroupCollector(ReflectionsFactory reflectionsFactory, MenuGroupFactory menuGroupFactory) {
        return new MenuGroupCollector(reflectionsFactory, menuGroupFactory);
    }

    @Bean
    @ConditionalOnMissingBean
    public MenuLayoutFactory menuLayoutFactory(ReflectionsFactory reflectionsFactory, MenuGroupCollector menuGroupCollector) {
        return new MenuLayoutFactory(reflectionsFactory, menuGroupCollector);
    }
}
