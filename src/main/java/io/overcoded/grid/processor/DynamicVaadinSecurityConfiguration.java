package io.overcoded.grid.processor;

import io.overcoded.grid.GridSecurityConfigurationProperties;
import io.overcoded.grid.annotation.GridSystem;
import io.overcoded.grid.security.GridRole;
import io.overcoded.grid.security.GridUser;
import io.overcoded.grid.security.GridUserActivity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@Configuration(proxyBeanMethods = false)
public class DynamicVaadinSecurityConfiguration implements InitializingBean {
    private final GridSecurityConfigurationProperties properties;
    private final ReflectionsFactory reflectionsFactory;

    @Override
    public void afterPropertiesSet() {
        Set<Class<?>> basePackage = reflectionsFactory.create().getTypesAnnotatedWith(GridSystem.class);
        if (!basePackage.isEmpty()) {
            Reflections reflections = reflectionsFactory.create(basePackage.stream().findFirst().orElse(null));
            Set<Class<? extends GridUser>> gridUsers = reflections.getSubTypesOf(GridUser.class);
            Set<Class<? extends GridRole>> gridRoles = reflections.getSubTypesOf(GridRole.class);
            Set<Class<? extends GridUserActivity>> gridUserActivities = reflections.getSubTypesOf(GridUserActivity.class);
            if (Objects.isNull(properties.getUserEntity())) {
                Class<? extends GridUser> userEntity = gridUsers.stream().findAny().orElse(null);
                log.debug("Found GridUsers: {}. {} will be used", gridUsers, userEntity);
                properties.setUserEntity(userEntity);
            }
            if (Objects.isNull(properties.getRoleEntity())) {
                Class<? extends GridRole> roleEntity = gridRoles.stream().findAny().orElse(null);
                log.debug("Found GridRoles: {}. {} will be used", gridRoles, roleEntity);
                properties.setRoleEntity(roleEntity);
            }
            if (Objects.isNull(properties.getUserActivityEntity())) {
                Class<? extends GridUserActivity> userActivityEntity = gridUserActivities.stream().findAny().orElse(null);
                log.debug("Found GridUserActivities: {}. {} will be used", gridUserActivities, userActivityEntity);
                properties.setUserActivityEntity(userActivityEntity);
            }
        }
    }
}
