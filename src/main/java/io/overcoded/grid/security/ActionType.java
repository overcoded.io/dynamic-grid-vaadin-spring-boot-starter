package io.overcoded.grid.security;

public enum ActionType {
    CREATE, MODIFY, DELETE
}
