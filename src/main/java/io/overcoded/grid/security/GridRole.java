package io.overcoded.grid.security;

public interface GridRole {
    String getName();
}
