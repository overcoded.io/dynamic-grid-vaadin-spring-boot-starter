package io.overcoded.grid.security;

import io.overcoded.grid.GridSecurityConfigurationProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class GridSecurityRepositories {
    private final GridSecurityConfigurationProperties properties;
    private final Repositories repositories;

    public <T extends GridUser> Optional<JpaRepository<T, Long>> getGridUserRepository() {
        return repositories
                .getRepositoryFor(properties.getUserEntity())
                .map(repository -> (JpaRepository<T, Long>) repository);
    }

    public <T extends GridUserActivity<U>, U extends GridUser> Optional<JpaRepository<T, Long>> getGridUserActivityRepository() {
        return repositories
                .getRepositoryFor(properties.getUserActivityEntity())
                .map(repository -> (JpaRepository<T, Long>) repository);
    }
}
