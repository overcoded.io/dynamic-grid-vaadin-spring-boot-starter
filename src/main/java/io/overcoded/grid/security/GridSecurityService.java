package io.overcoded.grid.security;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class GridSecurityService {
    private final LogoutHandler logoutHandler;

    public String getDisplayName() {
        String name = null;
        GridUser user = getAuthenticatedUser();
        if (Objects.nonNull(user)) {
            name = Optional.ofNullable(user.getFullName())
                    .or(() -> Optional.ofNullable(user.getUsername()))
                    .or(() -> Optional.ofNullable(user.getEmail()))
                    .orElse(null);
        }
        return name;
    }

    public GridUser getAuthenticatedUser() {
        return Optional.ofNullable(getAuthenticatedUserDetails())
                .map(GridUserDetails::getUser)
                .orElse(null);
    }

    public GridUserDetails getAuthenticatedUserDetails() {
        SecurityContext context = SecurityContextHolder.getContext();
        Object principal = context.getAuthentication().getPrincipal();
        if (principal instanceof GridUserDetails userDetails) {
            return userDetails;
        } else {
            log.error("Not a GridUserDetails");
        }
        // Anonymous or no authentication.
        return null;
    }

    public boolean hasPermission(List<String> requiredRoles) {
        Optional<GridUserDetails> currentUser = Optional.ofNullable(getAuthenticatedUserDetails());
        boolean result = Objects.isNull(requiredRoles) || requiredRoles.isEmpty();
        if (!result && currentUser.isPresent()) {
            List<GridRole> roles = (List<GridRole>) currentUser
                    .map(GridUserDetails::getUser)
                    .map(GridUser::getRoles)
                    .orElse(List.of());
            result = roles.stream().map(GridRole::getName).anyMatch(requiredRoles::contains);
        }
        return result;
    }

    public void logout() {
        try {
            UI.getCurrent().getPage().setLocation("/");
            logoutHandler.logout(VaadinServletRequest.getCurrent().getHttpServletRequest(), null, null);
        } catch (IllegalStateException ex) {
            log.debug("Failed to invalidate session", ex);
        }
    }
}
