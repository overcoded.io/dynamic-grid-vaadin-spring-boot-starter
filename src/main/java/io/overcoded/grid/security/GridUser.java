package io.overcoded.grid.security;

import jakarta.validation.constraints.NotNull;
import java.util.List;

public interface GridUser {
    String getEmail();

    String getUsername();

    String getFullName();

    void setUsername(@NotNull String username);

    String getPassword();

    void setPassword(@NotNull String password);

    boolean isEnabled();

    void setEnabled(boolean enabled);

    List<? extends GridRole> getRoles();

    List<? extends GridUserActivity<? extends GridUser>> getActivities();
}
