package io.overcoded.grid.security;

import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

public interface GridUserActivity<T extends GridUser> {

    T getUser();

    ActionType getActionType();

    String getEntityType();

    Long getEntityId();

    String getDifference();

    LocalDateTime getTimestamp();

    void setUser(@NotNull T user);

    void setActionType(@NotNull ActionType actionType);

    void setEntityType(@NotNull String entityType);

    void setEntityId(@NotNull Long id);

    void setDifference(String difference);

    void setTimestamp(@NotNull LocalDateTime timestamp);
}
