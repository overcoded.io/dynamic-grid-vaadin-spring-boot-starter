package io.overcoded.grid.security;

import io.overcoded.grid.GridSecurityConfigurationProperties;
import io.overcoded.grid.util.DifferenceCalculator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

@Slf4j
@Service
@RequiredArgsConstructor
public class GridUserActivityService {
    private final GridSecurityService securityService;
    private final DifferenceCalculator differenceCalculator;
    private final GridSecurityRepositories securityRepositories;
    private final GridSecurityConfigurationProperties properties;

    public <T extends GridUserActivity<U>, U extends GridUser> List<T> getPastActivities() {
        Optional<Example<GridUserActivity<GridUser>>> exampleUserActivity = getExampleUserActivity();
        List<T> activities = List.of();
        if (exampleUserActivity.isPresent()) {
            activities = (List<T>) securityRepositories.getGridUserActivityRepository()
                    .map(repository -> repository.findAll(exampleUserActivity.get(), getPageRequest()))
                    .map(Slice::getContent)
                    .orElseGet(List::of);
        }
        return activities;
    }

    public <T> void logCreate(T newValue, Long id) {
        logIfEnabled(() -> saveActivity(ActionType.CREATE, newValue, id));
    }

    public <T> void logUpdate(T newValue, Long id, Function<Long, Optional<T>> findById) {
        logIfEnabled(() -> {
            String differences = calculateDifferences(newValue, id, findById);
            saveActivity(ActionType.MODIFY, newValue, id, differences);
        });
    }

    public <T> void logDelete(T oldValue, Long id) {
        logIfEnabled(() -> saveActivity(ActionType.DELETE, oldValue, id));
    }

    private <T> void logIfEnabled(Runnable runnable) {
        if (properties.isActivityTrackingEnabled()) {
            if (properties.isActivityTrackingOnBackgroundThread()) {
                CompletableFuture.runAsync(runnable);
            } else {
                runnable.run();
            }
        }
    }

    private <T> String calculateDifferences(T newValue, Long id, Function<Long, Optional<T>> findById) {
        String difference = null;
        if (properties.isDifferenceTrackingEnabled()) {
            Optional<T> storedValue = findById.apply(id);
            if (storedValue.isPresent()) {
                difference = differenceCalculator.calculate(storedValue.get(), newValue);
            } else {
                log.error("Failed to fetch entity from database!");
            }
        }
        return difference;
    }

    private GridUser getAuthenticatedUser() {
        return securityService.getAuthenticatedUser();
    }

    private <T extends GridUserActivity<U>, U extends GridUser, E> void saveActivity(ActionType actionType, E entity, Long id) {
        saveActivity(actionType, entity, id, null);
    }

    private <T extends GridUserActivity<U>, U extends GridUser, E> void saveActivity(ActionType actionType, E entity, Long id, String difference) {
        String entityType = entity.getClass().getSimpleName();
        Optional<T> userActivity = getExampleUserActivity(actionType, entityType, id, difference);
        if (userActivity.isPresent()) {
            securityRepositories.getGridUserActivityRepository()
                    .map(repository -> repository.save((GridUserActivity<GridUser>) userActivity.get()));
        } else {
            log.error("Failed to create GridUserActivity instance!");
        }
    }

    private <T extends GridUserActivity<U>, U extends GridUser> Optional<T> getExampleUserActivity(ActionType actionType, String entityType, Long id) {
        return getExampleUserActivity(actionType, entityType, id, null);
    }

    private <T extends GridUserActivity<U>, U extends GridUser> Optional<T> getExampleUserActivity(ActionType actionType, String entityType, Long id, String difference) {
        Optional<T> result = Optional.empty();
        try {
            T activity = (T) properties.getUserActivityEntity().getConstructor().newInstance();
            activity.setUser((U) getAuthenticatedUser());
            activity.setActionType(actionType);
            activity.setEntityType(entityType);
            activity.setEntityId(id);
            activity.setDifference(difference);
            activity.setTimestamp(LocalDateTime.now());
            result = Optional.of(activity);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException |
                 IllegalAccessException e) {
            log.warn("Default no-arg constructor is required for {}", properties.getUserEntity());
        }
        return result;
    }

    private <T extends GridUserActivity<U>, U extends GridUser> Optional<Example<T>> getExampleUserActivity() {
        Optional<Example<T>> result = Optional.empty();
        try {
            T activity = (T) properties.getUserActivityEntity().getConstructor().newInstance();
            activity.setUser((U) getAuthenticatedUser());
            result = Optional.of(Example.of(activity));
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException |
                 IllegalAccessException e) {
            log.warn("Default no-arg constructor is required for {}", properties.getUserEntity());
        }
        return result;
    }

    private PageRequest getPageRequest() {
        return PageRequest.of(0, properties.getActivityHistorySize(), Sort.Direction.DESC, "timestamp");
    }
}
