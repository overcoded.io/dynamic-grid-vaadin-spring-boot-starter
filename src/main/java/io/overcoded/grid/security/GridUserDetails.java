package io.overcoded.grid.security;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Data
public class GridUserDetails implements UserDetails {
    private Collection<? extends GrantedAuthority> authorities;
    private GridUser user;
    private String password;
    private String username;
    private boolean enabled;

    public GridUserDetails(GridUser user) {
        this.user = user;
        this.password = user.getPassword();
        this.username = user.getUsername();
        this.enabled = user.isEnabled();
        this.authorities = Optional.ofNullable(user.getRoles())
                .orElseGet(List::of)
                .stream()
                .map(GridRole::getName)
                .map(SimpleGrantedAuthority::new)
                .toList();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
}
