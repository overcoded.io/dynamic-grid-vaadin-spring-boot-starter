package io.overcoded.grid.security;

import io.overcoded.grid.GridSecurityConfigurationProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

@Slf4j
@Transactional
@Component("userDetailsManager")
public class GridUserDetailsManager extends GridUserDetailsService implements UserDetailsManager, UserDetailsPasswordService {
    private final Pattern bcryptPattern = Pattern.compile("\\A\\$2a?\\$\\d\\d\\$[./0-9A-Za-z]{53}");
    private final PasswordEncoder passwordEncoder;

    public GridUserDetailsManager(GridSecurityConfigurationProperties gridSecurity,
                                  GridSecurityRepositories repositories,
                                  PasswordEncoder passwordEncoder) {
        super(gridSecurity, repositories);
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void createUser(UserDetails user) {
        if (!userExists(user.getUsername())) {
            if (user instanceof GridUserDetails userDetails) {
                GridUser gridUser = userDetails.getUser();
                createUser(gridUser);
            }
        }
    }

    public <T extends GridUser> T createUser(T gridUser) {
        gridUser.setPassword(passwordEncoder.encode(gridUser.getPassword()));
        return saveGridUser(gridUser);
    }

    @Override
    public void updateUser(UserDetails user) {
        if (userExists(user.getUsername())) {
            if (user instanceof GridUserDetails userDetails) {
                GridUser gridUser = userDetails.getUser();
                updateUser(gridUser);
            }
        }
    }

    public <T extends GridUser> T updateUser(T gridUser) {
        if (!bcryptPattern.matcher(gridUser.getPassword()).matches()) {
            gridUser.setPassword(passwordEncoder.encode(gridUser.getPassword()));
        }
        return saveGridUser(gridUser);
    }

    @Override
    public void deleteUser(String username) {
        if (userExists(username)) {
            deleteGridUser(username);
        }
    }


    @Override
    public UserDetails updatePassword(UserDetails user, String newPassword) {
        if (user instanceof GridUserDetails userDetails) {
            GridUser gridUser = userDetails.getUser();
            gridUser.setPassword(passwordEncoder.encode(newPassword));
            saveGridUser(gridUser);
        }
        return user;
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        getCurrentUser().ifPresent(userDetails -> {
            if (passwordEncoder.matches(oldPassword, userDetails.getPassword())) {
                updatePassword(userDetails, newPassword);
            } else {
                log.warn("Old password is not matching with the stored one.");
                throw new IllegalArgumentException("Old password is not matching with the stored one.");
            }
        });
    }

    @Override
    public boolean userExists(String username) {
        boolean result = false;
        try {
            result = Objects.nonNull(loadUserByUsername(username));
        } catch (UsernameNotFoundException ex) {
            // valid case, when user is not present
        }
        return result;
    }

    Optional<GridUserDetails> getCurrentUser() {
        Optional<GridUserDetails> result = Optional.empty();
        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
        if (Objects.nonNull(currentUser)) {
            String username = currentUser.getName();
            if (userExists(username)) {
                result = Optional.of(loadUserByUsername(username));
            }
        }
        return result;
    }

    private <T extends GridUser> T saveGridUser(T gridUser) {
        return repositories.getGridUserRepository().map(repository -> repository.save(gridUser)).get();
    }

    private void deleteGridUser(String username) {
        GridUserDetails userDetails = loadUserByUsername(username);
        GridUser user = userDetails.getUser();
        deleteUser(user);
    }

    public <T extends GridUser> void deleteUser(T user) {
        repositories.getGridUserRepository().ifPresent(repository -> repository.delete(user));
    }
}
