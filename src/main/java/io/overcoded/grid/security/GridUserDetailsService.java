package io.overcoded.grid.security;

import io.overcoded.grid.GridSecurityConfigurationProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
public class GridUserDetailsService implements UserDetailsService {
    protected final GridSecurityConfigurationProperties properties;
    protected final GridSecurityRepositories repositories;

    @Override
    @Transactional(readOnly = true)
    public GridUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Example<GridUser>> exampleUser = getExampleUser(username);
        GridUserDetails userDetails = null;
        if (exampleUser.isPresent()) {
            Optional<JpaRepository<GridUser, Long>> gridUserRepository = repositories.getGridUserRepository();
            userDetails = gridUserRepository
                    .flatMap(repository -> repository.findOne(exampleUser.get()))
                    .map(GridUserDetails::new)
                    .orElseThrow(() -> new UsernameNotFoundException(username + " not found."));

        }
        return userDetails;
    }


    private <T extends GridUser> Optional<Example<T>> getExampleUser(String username) {
        Optional<Example<T>> result = Optional.empty();
        try {
            T gridUser = (T) properties.getUserEntity().getConstructor().newInstance();
            gridUser.setUsername(username);
            gridUser.setEnabled(true);
            result = Optional.of(Example.of(gridUser, ExampleMatcher.matchingAll()));
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException |
                 IllegalAccessException e) {
            log.warn("Default no-arg constructor is required for {}", properties.getUserEntity());
        }
        return result;
    }

}
