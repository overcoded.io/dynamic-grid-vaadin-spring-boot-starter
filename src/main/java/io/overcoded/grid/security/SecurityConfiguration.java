package io.overcoded.grid.security;

import com.vaadin.flow.spring.security.VaadinWebSecurity;
import io.overcoded.grid.GridSecurityConfigurationProperties;
import io.overcoded.vaadin.LoginView;
import lombok.RequiredArgsConstructor;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration extends VaadinWebSecurity {
    private final GridSecurityConfigurationProperties gridSecurityConfigurationProperties;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests((authorizeHttpRequests) ->
                authorizeHttpRequests.requestMatchers("/public/**").permitAll()
        );
        super.configure(http);
        setLoginView(http, LoginView.class);
    }

    @Bean
    @ConditionalOnMissingBean
    public Javers javers() {
        return JaversBuilder.javers()
                .withListCompareAlgorithm(gridSecurityConfigurationProperties.getListCompareAlgorithm())
                .build();
    }

    @Bean
    @ConditionalOnMissingBean
    public Parser parser() {
        return Parser.builder().build();
    }

    @Bean
    @ConditionalOnMissingBean
    public HtmlRenderer htmlRenderer() {
        return HtmlRenderer.builder().build();
    }

    @Bean
    @ConditionalOnMissingBean
    public LogoutHandler logoutHandler() {
        return new SecurityContextLogoutHandler();
    }

    @Bean
    @ConditionalOnMissingBean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
