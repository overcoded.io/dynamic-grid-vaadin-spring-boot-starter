package io.overcoded.grid.util;

import lombok.RequiredArgsConstructor;
import org.javers.core.Javers;
import org.javers.core.diff.Diff;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DifferenceCalculator {
    private final Javers javers;

    public <T> String calculate(T originalEntity, T actualEntity) {
        Diff diff = javers.compare(originalEntity, actualEntity);
        StringBuilder builder = new StringBuilder();
        diff.groupByObject().forEach(it -> builder.append(it.toString()));
        return builder.toString();
    }
}
