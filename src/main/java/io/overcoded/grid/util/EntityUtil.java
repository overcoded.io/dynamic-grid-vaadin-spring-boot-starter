package io.overcoded.grid.util;

import io.overcoded.grid.processor.FieldCollector;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
@RequiredArgsConstructor
public class EntityUtil {
    private final Map<Class<?>, Field> idFields = new ConcurrentHashMap<>();
    private final FieldCollector fieldCollector;

    public <T> Long extractId(T value) {
        Long result = null;
        Class<?> entityType = value.getClass();
        Field field = getIdField(entityType);
        if (Objects.nonNull(field)) {
            try {
                field.setAccessible(true);
                result = (Long) field.get(value);
                field.setAccessible(false);
            } catch (IllegalAccessException e) {
                log.warn("Failed to extract id!");
            }
        }
        return result;
    }

    private Optional<Field> findIdField(Class<?> entityType) {
        return fieldCollector
                .getFields(entityType)
                .stream()
                .filter(field -> field.getName().equals("id"))
                .findFirst();
    }

    private Field getIdField(Class<?> entityType) {
        Field field = null;
        if (!idFields.containsKey(entityType)) {
            Optional<Field> idField = findIdField(entityType);
            if (idField.isPresent()) {
                field = idField.get();
                idFields.put(entityType, field);
            }
        } else {
            field = idFields.get(entityType);
        }
        return field;
    }
}
