package io.overcoded.grid.util;

import lombok.RequiredArgsConstructor;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MarkdownParser {
    private final Parser parser;
    private final HtmlRenderer htmlRenderer;

    public String toHtml(String markdown) {
        return htmlRenderer.render(parser.parse(markdown));
    }
}
