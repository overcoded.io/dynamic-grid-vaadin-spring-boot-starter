package io.overcoded.grid.util;

import io.overcoded.grid.MenuEntry;
import io.overcoded.grid.MenuGroup;
import io.overcoded.grid.MenuLayout;
import io.overcoded.grid.processor.MenuLayoutFactory;
import io.overcoded.grid.security.GridSecurityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class SecuredMenuEntryFinder {
    private final MenuLayoutFactory menuLayoutFactory;
    private final GridSecurityService securityService;

    public Optional<MenuEntry> findClosest(String path) {
        Optional<MenuEntry> menuEntry = find(path);
        if (menuEntry.isEmpty()) {
            menuEntry = parentPath(path).flatMap(this::findClosest);
        }
        return menuEntry;
    }

    public Optional<MenuEntry> findParent(MenuEntry menuEntry) {
        return getAllMenuEntries().stream()
                .filter(parent -> parent.getEntries().contains(menuEntry))
                .findFirst();
    }

    public List<MenuEntry> findChildren(MenuEntry parentEntry) {
        return parentEntry.getEntries()
                .stream()
                .filter(menuEntry -> securityService.hasPermission(menuEntry.getEnabledFor()))
                .toList();
    }

    public Optional<MenuEntry> find(String path) {
        List<MenuEntry> menuEntries = getAllMenuEntries();
        Optional<MenuEntry> result = findInMenuEntries(path, menuEntries);
        if (result.isEmpty()) {
            result = findInSubmenus(path, menuEntries);
        }
        return result;
    }

    private List<MenuEntry> getAllMenuEntries() {
        return menuLayoutFactory.create()
                .map(MenuLayout::getGroups)
                .orElse(List.of())
                .stream()
                .filter(menuGroup -> securityService.hasPermission(menuGroup.getEnabledFor()))
                .map(MenuGroup::getEntries)
                .flatMap(Collection::stream)
                .filter(menuEntry -> securityService.hasPermission(menuEntry.getEnabledFor()))
                .toList();
    }

    private Optional<MenuEntry> findInMenuEntries(String path, List<MenuEntry> menuEntries) {
        return menuEntries
                .stream()
                .filter(menuEntry -> menuEntry.getPath().equals(path))
                .findFirst();
    }

    private Optional<MenuEntry> findInSubmenus(String path, List<MenuEntry> menuEntries) {
        return menuEntries
                .stream()
                .map(MenuEntry::getEntries)
                .flatMap(Collection::stream)
                .filter(menuEntry -> menuEntry.getPath().equals(path))
                .findFirst();
    }

    private Optional<String> parentPath(String path) {
        Optional<String> result = Optional.empty();
        if (path.contains("/") && path.length() > 1) {
            result = Optional.of(path.substring(0, path.lastIndexOf("/")));
        }
        return result;
    }
}
