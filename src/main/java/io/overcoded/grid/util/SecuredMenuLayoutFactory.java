package io.overcoded.grid.util;

import io.overcoded.grid.MenuEntry;
import io.overcoded.grid.MenuGroup;
import io.overcoded.grid.MenuLayout;
import io.overcoded.grid.processor.MenuLayoutFactory;
import io.overcoded.grid.security.GridSecurityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SecuredMenuLayoutFactory {
    private final MenuLayoutFactory menuLayoutFactory;
    private final GridSecurityService securityService;

    public MenuLayout create() {
        MenuLayout menuLayout = menuLayoutFactory.create().orElseThrow();

        List<MenuGroup> groups = menuLayout
                .getGroups()
                .stream()
                .filter(menuGroup -> securityService.hasPermission(menuGroup.getEnabledFor()))
                .map(this::clone)
                .filter(menuGroup -> !menuGroup.getEntries().isEmpty())
                .toList();

        return MenuLayout.builder()
                .logo(menuLayout.getLogo())
                .title(menuLayout.getTitle())
                .type(menuLayout.getType())
                .groups(groups)
                .build();
    }

    private MenuGroup clone(MenuGroup menuGroup) {
        List<MenuEntry> entries = menuGroup.getEntries()
                .stream()
                .filter(menuEntry -> securityService.hasPermission(menuEntry.getEnabledFor()))
                .map(this::clone)
                .toList();
        return MenuGroup.builder()
                .enabledFor(menuGroup.getEnabledFor())
                .icon(menuGroup.getIcon())
                .path(menuGroup.getPath())
                .order(menuGroup.getOrder())
                .label(menuGroup.getLabel())
                .type(menuGroup.getType())
                .entries(entries)
                .build();
    }

    private MenuEntry clone(MenuEntry menuEntry) {
        List<MenuEntry> menuEntries = menuEntry.getEntries()
                .stream()
                .filter(submenuEntry -> securityService.hasPermission(submenuEntry.getEnabledFor()))
                .toList();
        return MenuEntry.builder()
                .enabledFor(menuEntry.getEnabledFor())
                .entries(menuEntries)
                .type(menuEntry.getType())
                .path(menuEntry.getPath())
                .icon(menuEntry.getIcon())
                .grouped(menuEntry.isGrouped())
                .label(menuEntry.getLabel())
                .order(menuEntry.getOrder())
                .build();
    }

}
