package io.overcoded.vaadin;

import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnConfigurationProperties;
import io.overcoded.grid.ColumnInfo;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

@SpringComponent
@RequiredArgsConstructor
public class BooleanBadgesFactory {
    private final ColumnConfigurationProperties properties;

    public <T> Icon createBadge(T instance, ColumnInfo info) {
        Boolean enabled = extractBooleanField(instance, info);
        Icon icon;
        if (Objects.isNull(enabled) && !properties.isFalsyValuesEnabled()) {
            icon = createIcon(VaadinIcon.MINUS, "Unknown");
            icon.getElement().getThemeList().add("badge contrast");
        } else {
            if (Objects.nonNull(enabled) && enabled) {
                icon = createIcon(VaadinIcon.CHECK, "Yes");
                icon.getElement().getThemeList().add("badge success");
            } else {
                icon = createIcon(VaadinIcon.CLOSE_SMALL, "No");
                icon.getElement().getThemeList().add("badge error");
            }
        }
        return icon;
    }

    private <T> Boolean extractBooleanField(T instance, ColumnInfo info) {
        Boolean result = null;
        if (Objects.nonNull(instance)) {
            try {
                Optional<Field> booleanField = Arrays.stream(instance.getClass().getDeclaredFields()).filter(field -> field.getName().equals(info.getName())).findFirst();
                if (booleanField.isPresent()) {
                    Field field = booleanField.get();
                    field.setAccessible(true);
                    result = (Boolean) field.get(instance);
                    field.setAccessible(false);
                }
            } catch (IllegalAccessException e) {
                // can be ignored
            }
        }
        return result;
    }

    private Icon createIcon(VaadinIcon vaadinIcon, String label) {
        Icon icon = vaadinIcon.create();
        icon.getStyle().set("padding", "var(--lumo-space-xs");
        icon.getElement().setAttribute("aria-label", label);
        icon.getElement().setAttribute("title", label);
        return icon;
    }
}
