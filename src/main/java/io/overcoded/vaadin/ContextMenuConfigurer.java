package io.overcoded.vaadin;

import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.contextmenu.SubMenuBase;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.menubar.MenuBarVariant;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.GridInfo;
import io.overcoded.grid.GridMenuGroup;
import io.overcoded.grid.GridMenuItem;
import io.overcoded.grid.security.GridSecurityService;
import io.overcoded.vaadin.grid.menu.GridMenuItemClickListenerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class ContextMenuConfigurer {
    private final GridMenuItemClickListenerFactory gridMenuItemClickListenerFactory;
    private final GridSecurityService securityService;

    public <T> void configure(GridCrud<T> gridCrud, GridInfo gridInfo) {
        List<GridMenuGroup> menuGroups = getEnabledMenuGroups(gridInfo);
        Grid<T> grid = gridCrud.getGrid();
        if (!menuGroups.isEmpty()) {
            Grid.Column<T> menuColumn = grid.addComponentColumn(item -> createMenuBar(gridCrud, menuGroups, item))
                    .setSortable(false)
                    .setAutoWidth(false)
                    .setWidth("72px")
                    .setTextAlign(ColumnTextAlign.END)
                    .setFrozen(true);
            List<Grid.Column<T>> newOrder = Stream.concat(Stream.of(menuColumn), grid.getColumns().stream().filter(tColumn -> !Objects.equals(menuColumn, tColumn))).toList();
            grid.setColumnOrder(newOrder);
        }
    }

    private List<GridMenuGroup> getEnabledMenuGroups(GridInfo gridInfo) {
        return gridInfo.getMenuGroups()
                .values()
                .stream()
                .sorted()
                .filter(group -> !getEnabledMenuEntries(group.getItems()).isEmpty())
                .toList();
    }

    private List<GridMenuItem> getEnabledMenuEntries(List<GridMenuItem> menuEntries) {
        return menuEntries.stream()
                .filter(entry -> securityService.hasPermission(entry.getEnabledFor()))
                .toList();
    }

    private <T> MenuBar createMenuBar(GridCrud<T> gridCrud, List<GridMenuGroup> menuGroups, T item) {
        MenuBar menuBar = new MenuBar();
        menuBar.addThemeVariants(MenuBarVariant.LUMO_ICON);
        addMenus(gridCrud, menuGroups, item, menuBar.addItem("•••"));
        return menuBar;
    }

    private <T> void addMenus(GridCrud<T> gridCrud, List<GridMenuGroup> menuGroups, T item, MenuItem menuItem) {
        // add entries without group
        menuGroups.stream()
                .filter(this::isDefaultGroup)
                .forEach(group -> addMenuEntries(gridCrud, item, menuItem, group.getItems()));
        // add grouped entries
        List<GridMenuGroup> groups = menuGroups.stream()
                .filter(Predicate.not(this::isDefaultGroup))
                .toList();
        if (!groups.isEmpty()) {
            addMenuGroups(gridCrud, item, menuItem, groups);
        }
    }

    private boolean isDefaultGroup(GridMenuGroup gridMenuGroup) {
        return Objects.isNull(gridMenuGroup.getName()) || gridMenuGroup.getName().isBlank();
    }

    private <T> void addMenuGroups(GridCrud<T> gridCrud, T item, MenuItem primaryMenuItem, List<GridMenuGroup> menuGroups) {
        SubMenu primarySubMenu = primaryMenuItem.getSubMenu();
        menuGroups.forEach(menuGroup -> {
            MenuItem menuItem = primarySubMenu.addItem(menuGroup.getName());
            addMenuEntries(gridCrud, item, menuItem, menuGroup.getItems());
            if (!hasMenuItem(menuItem)) {
                primarySubMenu.remove(menuItem);
            } else {
                findChildIcon(menuGroup.getIcon()).ifPresent(menuItem::addComponentAsFirst);
            }
        });
    }

    private boolean hasMenuItem(MenuItem menuItem) {
        return !Optional.ofNullable(menuItem.getSubMenu()).map(SubMenuBase::getItems).orElseGet(List::of).isEmpty();
    }

    private <T, U> void addMenuEntries(GridCrud<U> gridCrud, U item, MenuItem menuItem, List<GridMenuItem> menuEntries) {
        if (!menuEntries.isEmpty()) {
            SubMenu subMenu = menuItem.getSubMenu();
            menuEntries.stream()
                    .filter(entry -> securityService.hasPermission(entry.getEnabledFor()))
                    .forEach(entry -> {
                        MenuItem subMenuItem = addMenuItem(subMenu, entry);
                        subMenuItem.addClickListener(event -> {
                            gridMenuItemClickListenerFactory.trigger(gridCrud, item, entry, event.getSource().getUI().orElse(null), this::configure);
                        });
                    });
        }
    }

    private MenuItem addMenuItem(SubMenu subMenu, GridMenuItem entry) {
        if (entry.isDivided()) {
            subMenu.add(new Hr());
        }
        MenuItem subMenuItem = subMenu.addItem(entry.getLabel());
        findChildIcon(entry.getIcon()).ifPresent(subMenuItem::addComponentAsFirst);
        return subMenuItem;
    }

    private Optional<Icon> findChildIcon(String icon) {
        return findIcon(icon).map(vaadinIcon -> {
            vaadinIcon.getStyle().set("width", "var(--lumo-icon-size-s)");
            vaadinIcon.getStyle().set("height", "var(--lumo-icon-size-s)");
            vaadinIcon.getStyle().set("marginRight", "var(--lumo-space-s)");
            return vaadinIcon;
        });
    }

    private Optional<Icon> findIcon(String icon) {
        return Arrays.stream(VaadinIcon.values())
                .filter(vaadinIcon -> vaadinIcon.name().equalsIgnoreCase(icon))
                .map(VaadinIcon::create)
                .findFirst();
    }
}
