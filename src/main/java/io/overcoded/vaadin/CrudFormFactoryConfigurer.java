package io.overcoded.vaadin;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.CrudFormFactoryConfigurationProperties;
import io.overcoded.grid.GridInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;
import io.overcoded.vaadin.grid.field.EditorComponentFactory;
import lombok.RequiredArgsConstructor;
import org.vaadin.crudui.form.CrudFormFactory;
import org.vaadin.crudui.form.FieldProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@SpringComponent
@RequiredArgsConstructor
public class CrudFormFactoryConfigurer {
    private final EditorComponentFactory editorComponentFactory;
    private final CrudFormFactoryConfigurationProperties crudFormFactoryConfigurationProperties;

    <T, U> void configure(CrudFormFactory<T> crudFormFactory, GridInfo gridInfo, DynamicDialogParameter<T, U> dialogFilter) {
        crudFormFactory.setUseBeanValidation(true);
        crudFormFactory.setVisibleProperties(getColumnNames(gridInfo));
        if (crudFormFactoryConfigurationProperties.isDisableIdColumn()) {
            crudFormFactory.setDisabledProperties(getDisabledProperties(gridInfo).toArray(new String[]{}));
        }
        gridInfo.getColumns()
                .stream()
                .filter(this::isCustomField)
                .forEach(info -> configureFieldProvider(crudFormFactory, info, dialogFilter));
    }

    private List<String> getDisabledProperties(GridInfo gridInfo) {
        List<String> readOnlyProperties = gridInfo.getColumns().stream().filter(ColumnInfo::isReadOnly).map(ColumnInfo::getName).toList();
        List<String> idProperties = List.of(getIdProperty(gridInfo));
        List<String> disabledProperties = new ArrayList<>();
        disabledProperties.addAll(readOnlyProperties);
        disabledProperties.addAll(idProperties);
        return disabledProperties;
    }

    private String[] getIdProperty(GridInfo gridInfo) {
        return findIdField(gridInfo)
                .map(idField -> new String[]{idField})
                .orElseGet(() -> new String[]{});
    }

    private Optional<String> findIdField(GridInfo gridInfo) {
        return gridInfo.getColumns()
                .stream()
                .map(ColumnInfo::getName)
                .filter(name -> name.equals(crudFormFactoryConfigurationProperties.getIdColumnName()))
                .findFirst();
    }

    private String[] getColumnNames(GridInfo gridInfo) {
        return gridInfo.getColumns()
                .stream()
                .map(ColumnInfo::getName)
                .toList()
                .toArray(new String[gridInfo.getColumns().size()]);
    }

    private boolean isCustomField(ColumnInfo info) {
        return info.getFieldProviderType() != FieldProviderType.DEFAULT;
    }

    private <T, U> void configureFieldProvider(CrudFormFactory<T> crudFormFactory, ColumnInfo info, DynamicDialogParameter<T, U> dialogFilter) {
        crudFormFactory.setFieldProvider(info.getName(), (FieldProvider<AbstractField<?, ?>, Object>) o -> editorComponentFactory.createFormComponent(info, dialogFilter));
    }
}
