package io.overcoded.vaadin;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.GridInfo;
import lombok.RequiredArgsConstructor;
import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.layout.CrudLayout;

import java.util.List;

@SpringComponent
@RequiredArgsConstructor
public class CrudLayoutConfigurer {
    <T> void configure(CrudLayout crudLayout, GridCrud<T> gridCrud, GridInfo gridInfo, List<AbstractField<?, ?>> filterComponents) {
        filterComponents.forEach(component -> {
            component.addValueChangeListener(event -> gridCrud.refreshGrid());
            crudLayout.addFilterComponent(component);
        });
        Button clearSearch = new Button(new Icon(VaadinIcon.ERASER));
        clearSearch.addThemeVariants(ButtonVariant.LUMO_ICON);
        clearSearch.getElement().setAttribute("aria-label", "Add item");
        clearSearch.addClickListener(event -> filterComponents
                .stream()
                .filter(component -> !component.isReadOnly())
                .forEach(HasValue::clear));
        crudLayout.addFilterComponent(clearSearch);
        if (!gridInfo.isFormEnabled()) {
            crudLayout.hideForm();
        }
    }
}
