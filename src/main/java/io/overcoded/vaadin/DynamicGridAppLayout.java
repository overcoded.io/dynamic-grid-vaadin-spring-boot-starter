package io.overcoded.vaadin;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import io.overcoded.grid.MenuLayout;
import io.overcoded.grid.util.SecuredMenuEntryFinder;
import io.overcoded.grid.util.SecuredMenuLayoutFactory;
import io.overcoded.grid.security.GridSecurityService;
import io.overcoded.grid.security.GridUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class DynamicGridAppLayout extends AppLayout implements BeforeEnterObserver {
    private final MenuLayout menuLayout;
    private final DynamicNavigationBar navigationBar;
    private final DynamicGridDrawer drawer;

    public DynamicGridAppLayout(@Autowired SecuredMenuEntryFinder menuEntryFinder,
                                @Autowired GridSecurityService securityService,
                                @Autowired SecuredMenuLayoutFactory securedMenuLayoutFactory) {
        menuLayout = securedMenuLayoutFactory.create();
        navigationBar = new DynamicNavigationBar(menuEntryFinder);
        drawer = new DynamicGridDrawer(menuLayout.getGroups());
        configure(securityService.getAuthenticatedUser());
    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        String path = "/" + beforeEnterEvent.getLocation().getPath();
        navigationBar.navigate(path);
        drawer.activate(path);
    }

    /**
     * Configures AppLayout
     */
    private void configure(GridUser gridUser) {
        DynamicGridDrawerTitle title = new DynamicGridDrawerTitle(menuLayout, gridUser);
        HorizontalLayout drawerHolder = new HorizontalLayout(drawer);
        drawerHolder.setPadding(true);

        addToDrawer(title, drawerHolder);
        addToNavbar(navigationBar);

        setPrimarySection(Section.DRAWER);
        setDrawerOpened(true);
    }
}
