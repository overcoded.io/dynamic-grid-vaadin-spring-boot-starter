package io.overcoded.vaadin;


import com.vaadin.flow.component.accordion.Accordion;
import io.overcoded.grid.MenuGroup;

import java.util.List;

public class DynamicGridDrawer extends Accordion {
    private final List<DynamicGridMenuGroup> panels;

    public DynamicGridDrawer(List<MenuGroup> menuGroups) {
        panels = createMenuEntries(menuGroups);
        addMenuGroups();
        configure();
    }

    public void activate(String path) {
        panels.stream()
                .filter(menuGroup -> menuGroup.activate(path))
                .forEach(this::open);
    }

    private List<DynamicGridMenuGroup> createMenuEntries(List<MenuGroup> menuGroups) {
        return menuGroups
                .stream()
                .map(DynamicGridMenuGroup::new)
                .toList();
    }

    private void addMenuGroups() {
        panels.forEach(this::add);
    }

    /**
     * Configures AccordionPanel Vaadin component
     */
    private void configure() {
        close();
    }
}
