package io.overcoded.vaadin;

import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.avatar.AvatarVariant;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import io.overcoded.grid.MenuLayout;
import io.overcoded.grid.security.GridUser;

import java.util.Objects;

public class DynamicGridDrawerTitle extends VerticalLayout {
    private final MenuLayout menuLayout;

    public DynamicGridDrawerTitle(MenuLayout menuLayout, GridUser gridUser) {
        this.menuLayout = menuLayout;
        configure(gridUser);
    }

    /**
     * Configures  Vaadin component
     */
    private void configure(GridUser gridUser) {
        Avatar avatar = createAvatar(gridUser);
        Anchor anchor = new Anchor("/me", avatar);
        add(anchor);
        setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        setAlignSelf(Alignment.CENTER);
        setAlignItems(Alignment.CENTER);
    }

    private Avatar createAvatar(GridUser gridUser) {
        Avatar avatar = new Avatar(gridUser.getFullName());
        avatar.addThemeVariants(AvatarVariant.LUMO_XLARGE);
        if (Objects.nonNull(menuLayout.getLogo()) && !menuLayout.getLogo().isBlank()) {
            avatar.setImage(menuLayout.getLogo());
        }
        return avatar;
    }
}
