package io.overcoded.vaadin;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.accordion.AccordionPanel;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.tabs.Tab;
import io.overcoded.grid.MenuGroup;

/**
 * Represents a menu group on the drawer.
 */
public class DynamicGridMenuGroup extends AccordionPanel {
    private final VaadinMenuGroup vaadinMenuGroup;
    private final MenuGroup menuGroup;

    public DynamicGridMenuGroup(MenuGroup menuGroup) {
        this.menuGroup = menuGroup;
        this.vaadinMenuGroup = new VaadinMenuGroup(menuGroup);
        configure();
    }

    /**
     * Set the corresponding tab as active if it's available on this panel.
     * If path can be activated in the menu group, sets the panel opened.
     *
     * @param path which should be activated
     * @return true if the path available under this panel, false otherwise
     */
    public boolean activate(String path) {
        boolean containsPath = vaadinMenuGroup.activate(path);
        setOpened(containsPath);
        return containsPath;
    }

    /**
     * Configures AccordionPanel Vaadin component
     */
    private void configure() {
        setSummary(getSectionHead());
        setContent(vaadinMenuGroup);
    }

    private Tab getSectionHead() {
        Tab tab = new Tab(getIcon(), getLabel());
        tab.setSelected(false);
        return tab;
    }

    private Icon getIcon() {
        String icon = menuGroup.getIcon();
        return VaadinIcon.valueOf(icon).create();
    }

    private Text getLabel() {
        return new Text(menuGroup.getLabel());
    }
}
