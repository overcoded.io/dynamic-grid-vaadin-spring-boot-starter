package io.overcoded.vaadin;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.*;
import io.overcoded.grid.MenuEntry;
import io.overcoded.grid.security.GridSecurityService;
import io.overcoded.grid.util.SecuredMenuEntryFinder;
import io.overcoded.vaadin.panel.DynamicGridPanelFactory;
import jakarta.annotation.security.PermitAll;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.util.Objects;
import java.util.Optional;

@Slf4j
@Setter
@PermitAll
@PreserveOnRefresh
@Route(value = "/:path*", layout = DynamicGridAppLayout.class)
@CssImport(value = "./vaadin-grid-styles.css", themeFor = "vaadin-grid")
public class DynamicGridView extends HorizontalLayout implements BeforeEnterObserver, HasDynamicTitle {
    private final GridViewFactory gridViewFactory;
    private final DynamicGridPanelFactory gridPanelFactory;
    private final SecuredMenuEntryFinder menuEntryFinder;
    private final GridSecurityService securityService;
    private MenuEntry menuEntry;
    private String path;

    public DynamicGridView(@Autowired SecuredMenuEntryFinder menuEntryFinder,
                           @Autowired GridViewFactory gridViewFactory,
                           @Autowired DynamicGridPanelFactory gridPanelFactory,
                           @Autowired GridSecurityService gridSecurityService) {
        this.gridViewFactory = gridViewFactory;
        this.menuEntryFinder = menuEntryFinder;
        this.gridPanelFactory = gridPanelFactory;
        this.securityService = gridSecurityService;
    }

    @Override
    public String getPageTitle() {
        return Objects.isNull(menuEntry) ? "" : menuEntry.getLabel();
    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        setPath(calculatePageUrl(beforeEnterEvent));
        log.info("Loading dynamic content of: {}", path);
        setMenuEntry();
        configure();
    }

    private void setMenuEntry() {
        Optional<MenuEntry> loadedMenuEntry = menuEntryFinder.find(path);
        loadedMenuEntry.ifPresent(entry -> menuEntry = entry);
    }

    private String calculatePageUrl(BeforeEnterEvent event) {
        return ("/" + event.getRouteParameters().get("path").orElse("")).replaceAll("//", "/");
    }

    private void configure() {
        setSizeFull();
        removeAll();
        if (Objects.nonNull(menuEntry)) {
            if (securityService.hasPermission(menuEntry.getEnabledFor())) {
                GridCrud<?> gridCrud = gridViewFactory.create(menuEntry.getType());
                add(gridCrud);
            } else {
                add(gridPanelFactory.createError("Permission denied!", new Paragraph("You don't have the necessary permissions to open this view.")));
            }
        } else {
            add(gridPanelFactory.createError("Permission denied!", new Paragraph("You don't have the necessary permissions to open this view.")));
        }
    }
}
