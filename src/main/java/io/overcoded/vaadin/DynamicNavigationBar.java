package io.overcoded.vaadin;


import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.menubar.MenuBarVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import io.overcoded.grid.MenuEntry;
import io.overcoded.grid.util.SecuredMenuEntryFinder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DynamicNavigationBar extends HorizontalLayout {
    private final SecuredMenuEntryFinder menuEntryFinder;
    private final H2 viewTitle;
    private final MenuBar menuBar;

    public DynamicNavigationBar(SecuredMenuEntryFinder menuEntryFinder) {
        this.menuEntryFinder = menuEntryFinder;
        this.viewTitle = createTitle();
        this.menuBar = createMenuBar();
        configure();
    }

    public void navigate(String path) {
        log.info("Updating navigation bar to {}", path);
        menuEntryFinder.find(path).ifPresent(this::navigate);
    }

    public void navigate(MenuEntry menuEntry) {
        log.info("Updating navigation bar based on found menu entry for {}", menuEntry.getPath());
        cleanPreviousState();
        setTitle(menuEntry);
        if (hasSubmenus(menuEntry)) {
            configureMenuBar(menuEntry);
        } else {
            configureMenuBarWithParent(menuEntry);
        }
    }

    private H2 createTitle() {
        H2 title = new H2();
        title.getStyle()
                .set("font-size", "var(--lumo-font-size-l)")
                .set("margin-right", "8px")
                .set("margin", "0");
        return title;
    }

    private MenuBar createMenuBar() {
        MenuBar menuBar = new MenuBar();
        menuBar.addThemeVariants(MenuBarVariant.LUMO_END_ALIGNED);
        return menuBar;
    }

    private void cleanPreviousState() {
        menuBar.removeAll();
    }

    private void setTitle(MenuEntry menuEntry) {
        viewTitle.setText(menuEntry.getLabel());
    }

    private boolean hasSubmenus(MenuEntry menuEntry) {
        return !menuEntry.getEntries().isEmpty();
    }

    private void configureMenuBar(MenuEntry menuEntry) {
        menuEntryFinder.findChildren(menuEntry).forEach(this::addMenuEntry);
    }

    private void configureMenuBarWithParent(MenuEntry menuEntry) {
        menuEntryFinder.findParent(menuEntry).ifPresent(parent -> configureMenuBar(parent, menuEntry));
    }

    private void configureMenuBar(MenuEntry parentEntry, MenuEntry menuEntry) {
        addMenuEntry(parentEntry);
        menuEntryFinder.findChildren(parentEntry)
                .stream()
                .filter(entry -> !entry.equals(menuEntry))
                .forEach(this::addMenuEntry);
    }

    private void addMenuEntry(MenuEntry menuEntry) {
        MenuItem menuItem = menuBar.addItem(createIcon(menuEntry.getIcon()));
        menuItem.add(new Text(menuEntry.getLabel()));
        menuItem.addClickListener(event -> UI.getCurrent().navigate(menuEntry.getPath()));
    }

    private Icon createIcon(String icon) {
        return VaadinIcon.valueOf(icon).create();
    }

    /**
     * Configures  Vaadin component
     */
    private void configure() {
        DrawerToggle drawerToggle = new DrawerToggle();
        add(drawerToggle, viewTitle, menuBar);
        setAlignItems(Alignment.CENTER);
        setPadding(false);
        setSpacing(true);
        setWidthFull();
    }
}
