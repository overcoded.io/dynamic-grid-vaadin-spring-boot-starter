package io.overcoded.vaadin;

import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.processor.FieldCollector;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.support.Repositories;

import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class DynamicRelationshipService {
    private final Repositories repositories;
    private final FieldCollector fieldCollector;

    /**
     * @param <O> owner
     * @param <I> item
     */
    public <O, I> boolean save(O owner, I item, String property) {
        boolean result = false;
        Optional<Field> relationshipHolder = getRelationshipHolder(owner, property);
        if (relationshipHolder.isPresent()) {
            Field field = relationshipHolder.get();
            result = isOneToOneRelationShip(field) && updateOne(owner, item, field) && persist(owner);
        }
        return result;
    }

    public <O, I> boolean save(O owner, List<I> item, String property) {
        boolean result = false;
        Optional<Field> relationshipHolder = getRelationshipHolder(owner, property);
        if (relationshipHolder.isPresent()) {
            Field field = relationshipHolder.get();
            result = !isOneToOneRelationShip(field) && updateAll(owner, item, field) && persist(owner);
        }
        return result;
    }

    public <O, I> boolean delete(O owner, String property) {
        boolean result = false;
        Optional<Field> relationshipHolder = getRelationshipHolder(owner, property);
        if (relationshipHolder.isPresent()) {
            Field field = relationshipHolder.get();
            result = isOneToOneRelationShip(field) && removeOnly(owner, field) && persist(owner);
        }
        return result;
    }

    private <O> Optional<Field> getRelationshipHolder(O owner, String property) {
        return fieldCollector.getFields(owner.getClass())
                .stream()
                .filter(field -> field.getName().equals(property))
                .findFirst();
    }

    private boolean isOneToOneRelationShip(Field field) {
        return field.isAnnotationPresent(OneToOne.class);
    }

    private boolean hasManyToManyRelationship(Field field) {
        return field.isAnnotationPresent(ManyToMany.class);
    }

    private <O, I> boolean updateOne(O owner, I item, Field field) {
        return doWithField(field, (f) -> {
            try {
                field.set(owner, item);
            } catch (IllegalAccessException e) {
                log.warn("Failed to update one-to-one relationship between {} and {} with field {}", owner, item, f.getName());
                throw new IllegalCallerException(e);
            }
        });
    }

    private <O, I> boolean removeOnly(O owner, Field field) {
        return doWithField(field, (f) -> {
            try {
                field.set(owner, null);
            } catch (IllegalAccessException e) {
                log.warn("Failed to remove one-to-one relationship between {} with field {}", owner, f.getName());
                throw new IllegalCallerException(e);
            }
        });
    }

    private <O, I> boolean updateAll(O owner, List<I> items, Field field) {
        return doWithField(field, (f) -> {
            try {
                if (f.getType().isAssignableFrom(Set.class)) {
                    f.set(owner, new HashSet<>(items));
                } else {
                    f.set(owner, items);
                }
            } catch (IllegalAccessException e) {
                log.warn("Failed to update one-to-one relationship between {} and {} with field {}", owner, items, f.getName());
                throw new IllegalCallerException(e);
            }
        });
    }

    private <O, I> boolean removeOne(O owner, I item, Field field) {
        return doWithField(field, (f) -> {
            try {
                Collection<I> collection = (Collection<I>) f.get(owner);
                collection.remove(item);
            } catch (IllegalAccessException e) {
                log.warn("Failed to remove one-to-one relationship between {} and {} with field {}", owner, item, f.getName());
                throw new IllegalCallerException(e);
            }
        });
    }

    private boolean doWithField(Field field, Consumer<Field> consumer) {
        boolean result = true;
        try {
            field.setAccessible(true);
            consumer.accept(field);
            field.setAccessible(false);
        } catch (Exception ex) {
            log.debug("Failed to update one-to-one relationship", ex);
            result = false;
        }
        return result;
    }

    private <O> boolean persist(O owner) {
        boolean result = false;
        Optional<Object> repositoryFor = repositories.getRepositoryFor(owner.getClass());
        if (repositoryFor.isPresent()) {
            JpaRepository<O, Long> repository = (JpaRepository<O, Long>) repositoryFor.get();
            O saved = repository.save(owner);
        }
        return result;
    }

}
