package io.overcoded.vaadin;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.theme.lumo.LumoUtility;
import io.overcoded.grid.ColumnConfigurationProperties;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.GridInfo;
import io.overcoded.vaadin.grid.DynamicItemLabelGeneratorFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class GridConfigurer {
    private final ColumnConfigurationProperties columnConfigurationProperties;
    private final DynamicItemLabelGeneratorFactory itemLabelGeneratorFactory;
    private final BooleanBadgesFactory booleanAsBadgesEvaluator;
    private final ImagePreviewBadgeFactory imagePreviewBadgeFactory;

    <T> void configure(Grid<T> grid, GridInfo gridInfo) {
        grid.removeAllColumns();
        grid.setPageSize(columnConfigurationProperties.getPageSize());
        List<Grid.Column<T>> columns = gridInfo.getColumns()
                .stream()
                .map(info -> convertToColumn(grid, info))
                .toList();
        grid.setColumnReorderingAllowed(columnConfigurationProperties.isColumnReorderingAllowed());
        grid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
        configureEnabledColumn(grid, gridInfo);
        grid.setColumnOrder(columns);
        configureMultiSort(grid);
    }

    private <T> void configureMultiSort(Grid<T> grid) {
        if (columnConfigurationProperties.isMultiSortEnabled()) {
            grid.setMultiSort(true, Grid.MultiSortPriority.APPEND);
        }
    }

    private <T> Grid.Column<T> convertToColumn(Grid<T> grid, ColumnInfo info) {
        Grid.Column<T> column = createColumn(grid, info)
                .setAutoWidth(columnConfigurationProperties.isAutoWidthEnabled())
                .setSortable(columnConfigurationProperties.isSortable())
                .setResizable(columnConfigurationProperties.isResizable())
                .setHeader(info.getLabel());
        column.setVisible(!info.isHidden());
        return column;
    }

    private <T> Grid.Column<T> createColumn(Grid<T> grid, ColumnInfo info) {
        Grid.Column<T> column;
        if ((info.getType().isAssignableFrom(Boolean.class) && !info.getType().equals(Object.class)) && columnConfigurationProperties.isBadgesForBooleanEnabled()) {
            column = grid.addComponentColumn(value -> booleanAsBadgesEvaluator.createBadge(value, info));
        } else if (Objects.nonNull(info.getImagePreview())) {
            column = grid.addComponentColumn(value -> imagePreviewBadgeFactory.createButton(value, info));
        } else if (info.getDisplayValueExpressionParts().isEmpty()) {
            column = grid.addColumn(info.getName());
        } else {
            column = grid.addColumn(itemLabelGeneratorFactory.create(info.getName(), info.getDisplayValueExpressionParts()));
        }
        return column;
    }

    private <T> void configureEnabledColumn(Grid<T> grid, GridInfo gridInfo) {
        getEnabledColumn(gridInfo.getColumns()).ifPresent(info -> grid.setClassNameGenerator(entity -> {
            String className = null;
            try {
                Field field = grid.getBeanType().getDeclaredField(info.getName());
                field.setAccessible(true);
                Boolean enabled = (Boolean) field.get(entity);
                field.setAccessible(false);
                if (Objects.nonNull(enabled) && !enabled) {
                    className = LumoUtility.TextColor.DISABLED;
                }
            } catch (Exception ex) {
                log.error("Failed to configure enabled column! {}", ex.getMessage());
            }
            return className;
        }));
    }

    private Optional<ColumnInfo> getEnabledColumn(List<ColumnInfo> columns) {
        return columns.stream()
                .filter(columnInfo -> columnConfigurationProperties.getEnabledFields().contains(columnInfo.getName()))
                .filter(columnInfo -> boolean.class.equals(columnInfo.getType()) || Boolean.class.equals(columnInfo.getType()))
                .findFirst();
    }

}
