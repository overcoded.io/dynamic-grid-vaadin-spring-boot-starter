package io.overcoded.vaadin;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.GridInfo;
import io.overcoded.grid.annotation.GridMethod;
import io.overcoded.vaadin.grid.CustomCrudListenerFactory;
import io.overcoded.vaadin.grid.DynamicGridCrudCallback;
import io.overcoded.vaadin.grid.DynamicGridCrudListenerFactory;
import lombok.RequiredArgsConstructor;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

@SpringComponent
@RequiredArgsConstructor
public class GridCrudConfigurer {
    private final DynamicGridCrudListenerFactory dynamicGridCrudListenerFactory;
    private final CustomCrudListenerFactory customCrudListenerFactory;

    private final Map<GridMethod, Consumer<GridCrud<?>>> operationEnablers = Map.of(
            GridMethod.C, gridCrud -> gridCrud.setAddOperationVisible(true),
            GridMethod.U, gridCrud -> gridCrud.setUpdateOperationVisible(true),
            GridMethod.D, gridCrud -> gridCrud.setDeleteOperationVisible(true)
    );

    <T> void configure(GridCrud<T> gridCrud, GridInfo gridInfo, List<AbstractField<?, ?>> filterComponents, DynamicGridCrudCallback<T> gridCrudCallback) {
        dynamicGridCrudListenerFactory
                .create(gridInfo, filterComponents, gridCrudCallback)
                .ifPresentOrElse(gridCrud::setCrudListener, () -> configureCustomListener(gridCrud, filterComponents));
        configureCrudMethods(gridCrud, gridInfo.getEnabledMethods());
    }

    private <T> void configureCustomListener(GridCrud<T> gridCrud, List<AbstractField<?, ?>> filterComponents) {
        customCrudListenerFactory
                .findCrudListener(gridCrud.getGrid().getBeanType(), filterComponents)
                .ifPresent(gridCrud::setCrudListener);
    }

    private <T> void configureCrudMethods(GridCrud<T> gridCrud, List<GridMethod> methods) {
        if (Objects.nonNull(methods) && !methods.isEmpty()) {
            disableAllMethods(gridCrud);
            enableAllowedMethods(gridCrud, methods);
        }
    }

    private <T> void enableAllowedMethods(GridCrud<T> gridCrud, List<GridMethod> methods) {
        methods.stream()
                .map(operationEnablers::get)
                .filter(Objects::nonNull)
                .forEach(gridCrudConsumer -> gridCrudConsumer.accept(gridCrud));
    }

    private <T> void disableAllMethods(GridCrud<T> gridCrud) {
        gridCrud.setDeleteOperationVisible(false);
        gridCrud.setUpdateOperationVisible(false);
        gridCrud.setAddOperationVisible(false);
    }
}
