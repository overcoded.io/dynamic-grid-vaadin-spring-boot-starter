package io.overcoded.vaadin;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.GridCrudLayout;
import io.overcoded.grid.GridCrudProperties;
import io.overcoded.grid.GridInfo;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;
import io.overcoded.vaadin.grid.DynamicGridCrudCallback;
import io.overcoded.vaadin.grid.field.EditorComponentFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.crudui.crud.impl.GridCrud;
import org.vaadin.crudui.form.impl.form.factory.DefaultCrudFormFactory;
import org.vaadin.crudui.layout.CrudLayout;

import java.util.List;
import java.util.Objects;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class GridCrudFactory {
    private final GridCrudProperties properties;
    private final GridConfigurer gridConfigurer;
    private final GridCrudConfigurer gridCrudConfigurer;
    private final CrudLayoutConfigurer crudLayoutConfigurer;
    private final CrudFormFactoryConfigurer crudFormFactoryConfigurer;
    private final EditorComponentFactory editorComponentFactory;

    public <T> GridCrud<T> create(GridInfo gridInfo) {
        return create(gridInfo, null);
    }

    public <T, U> GridCrud<T> create(GridInfo gridInfo, DynamicDialogParameter<T, U> dialogFilter) {
        return create(gridInfo, dialogFilter, null);
    }

    public <T, U> GridCrud<T> create(GridInfo gridInfo, DynamicDialogParameter<T, U> dialogFilter, DynamicGridCrudCallback<T> gridCrudCallback) {
        log.info("Filter parameters: {}", dialogFilter);
        List<AbstractField<?, ?>> filterComponents = editorComponentFactory.createFilterComponents(gridInfo, dialogFilter);

        CrudLayout crudLayout = createLayout(dialogFilter, gridInfo);
        DefaultCrudFormFactory<T> crudFormFactory = createCrudFormFactory(gridInfo);
        GridCrud<T> gridCrud = new GridCrud<>((Class<T>) gridInfo.getType(), crudLayout, crudFormFactory);
        Grid<T> grid = gridCrud.getGrid();

        crudFormFactoryConfigurer.configure(crudFormFactory, gridInfo, dialogFilter);
        gridConfigurer.configure(grid, gridInfo);
        crudLayoutConfigurer.configure(crudLayout, gridCrud, gridInfo, filterComponents);
        gridCrudConfigurer.configure(gridCrud, gridInfo, filterComponents, gridCrudCallback);

        return gridCrud;
    }

    private <T> DefaultCrudFormFactory<T> createCrudFormFactory(GridInfo gridInfo) {
        return isSingleColumnRequired(gridInfo)
                ? new DefaultCrudFormFactory<>((Class<T>) gridInfo.getType(), new FormLayout.ResponsiveStep("0", 1))
                : new DefaultCrudFormFactory<>((Class<T>) gridInfo.getType());
    }

    private <T, U> CrudLayout createLayout(DynamicDialogParameter<T, U> parameter, GridInfo gridInfo) {
        GridCrudLayout layoutType;
        if (gridInfo.isFormEnabled()) {
            layoutType = Objects.isNull(parameter) ? properties.getLayout() : properties.getDialogLayout();
        } else {
            layoutType = properties.getWithoutFormLayout();
        }
        return layoutType.create();
    }

    private boolean isSingleColumnRequired(GridInfo gridInfo) {
        return properties.isSingleColumnOnly()
                || gridInfo.getColumns().stream()
                .map(ColumnInfo::getFieldProviderType)
                .anyMatch(fieldProviderType -> properties.getSingleColumnFieldProviderTypes().contains(fieldProviderType));
    }


}
