package io.overcoded.vaadin;

import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.GridInfo;
import io.overcoded.grid.processor.GridInfoViewFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.crudui.crud.impl.GridCrud;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class GridViewFactory {
    private final GridCrudFactory gridCrudFactory;
    private final ContextMenuConfigurer contextMenuConfigurer;
    private final GridInfoViewFactory gridInfoViewFactory;

    public <T> GridCrud<T> create(Class<T> type) {
        GridInfo gridInfo = gridInfoViewFactory.create(type);
        return create(gridInfo);
    }

    public <T> GridCrud<T> create(GridInfo gridInfo) {
        GridCrud<T> gridCrud = gridCrudFactory.create(gridInfo);
        contextMenuConfigurer.configure(gridCrud, gridInfo);
        return gridCrud;
    }
}
