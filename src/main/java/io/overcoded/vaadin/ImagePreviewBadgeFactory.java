package io.overcoded.vaadin;

import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.ImagePreview;
import io.overcoded.vaadin.dialog.Preview;
import io.overcoded.vaadin.grid.DynamicItemLabelGenerator;
import io.overcoded.vaadin.grid.DynamicItemLabelGeneratorFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class ImagePreviewBadgeFactory {
    private final ExpressionParser expressionParser;
    private final DynamicItemLabelGeneratorFactory dynamicItemLabelGeneratorFactory;

    public <T> HorizontalLayout createButton(T instance, ColumnInfo info) {
        ImagePreview imagePreview = resolvedPreview(instance, info);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(new Preview(extractStringField(instance, info), imagePreview.getPreviewLink(), imagePreview.getLink()));
        horizontalLayout.setAlignItems(FlexComponent.Alignment.BASELINE);
        return horizontalLayout;
    }

    private <T> String extractStringField(T instance, ColumnInfo info) {
        String result = "";
        if (Objects.nonNull(instance)) {
            if (Objects.nonNull(info.getDisplayValueExpressionParts()) && !info.getDisplayValueExpressionParts().isEmpty()) {
                DynamicItemLabelGenerator<T> dynamicItemLabelGenerator = dynamicItemLabelGeneratorFactory.create(info.getName(), info.getDisplayValueExpressionParts());
                result = dynamicItemLabelGenerator.apply(instance);
            } else {
                try {
                    Optional<Field> stringField = Arrays.stream(instance.getClass().getDeclaredFields()).filter(field -> field.getName().equals(info.getName())).findFirst();
                    if (stringField.isPresent()) {
                        Field field = stringField.get();
                        field.setAccessible(true);
                        result = String.valueOf(field.get(instance));
                        field.setAccessible(false);
                    }
                } catch (IllegalAccessException e) {
                    // can be ignored
                }
            }
        }
        return result;
    }

    private <T> ImagePreview resolvedPreview(T instance, ColumnInfo columnInfo) {
        List<String> arguments = getArguments(instance, columnInfo);
        Object[] argumentsArray = arguments.toArray(new Object[]{});

        return ImagePreview.builder()
                .maxWidth(columnInfo.getImagePreview().getMaxWidth())
                .maxHeight(columnInfo.getImagePreview().getMaxHeight())
                .link(columnInfo.getImagePreview().getLink().formatted(argumentsArray))
                .previewLink(columnInfo.getImagePreview().getPreviewLink().formatted(argumentsArray))
                .build();
    }

    private <T> List<String> getArguments(T instance, ColumnInfo columnInfo) {
        List<String> result = List.of();
        try {
            EvaluationContext context = new StandardEvaluationContext(instance);
            result = columnInfo.getImagePreview().getArguments().stream()
                    .map(expressionParser::parseExpression)
                    .map(expression -> expression.getValue(context))
                    .map(String::valueOf)
                    .collect(Collectors.toList());
        } catch (EvaluationException ex) {
            log.trace("SpelExpression apply failed. {}", ex.getMessage());
        }
        return result;
    }
}
