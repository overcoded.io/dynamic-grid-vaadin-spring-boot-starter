package io.overcoded.vaadin;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Route(value = "/login")
@CssImport("./login-rich-content.css")
public class LoginView extends VerticalLayout implements BeforeEnterObserver {
    private final LoginForm loginForm = new LoginForm();

    public LoginView() {
        setSizeFull();
        setMargin(false);
        setPadding(false);
        addClassName("login-rich-content");

        LoginForm loginForm = new LoginForm();
        loginForm.setAction("login");
        loginForm.getElement().getThemeList().add("dark");

        add(loginForm);
    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (beforeEnterEvent.getLocation()
                .getQueryParameters()
                .getParameters()
                .containsKey("error")) {
            loginForm.setError(true);
            DynamicErrorNotification notification = new DynamicErrorNotification("Authentication failed. Please check your username and password.");
            notification.open();
        }
    }
}
