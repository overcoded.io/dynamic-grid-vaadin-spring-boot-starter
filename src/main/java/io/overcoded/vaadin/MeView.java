package io.overcoded.vaadin;

import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;
import io.overcoded.vaadin.user.PasswordChangeLayoutFactory;
import io.overcoded.vaadin.user.UserActivityLayoutFactory;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import jakarta.annotation.security.PermitAll;

@Slf4j
@Setter
@PermitAll
@PageTitle("Me")
@PreserveOnRefresh
@Route(value = "/me", layout = StaticGridAppLayout.class)
public class MeView extends VerticalLayout {
    private final UserActivityLayoutFactory userActivityLayoutFactory;
    private final PasswordChangeLayoutFactory passwordChangeLayoutFactory;

    public MeView(@Autowired UserActivityLayoutFactory userActivityLayoutFactory,
                  @Autowired PasswordChangeLayoutFactory passwordChangeLayoutFactory) {
        this.userActivityLayoutFactory = userActivityLayoutFactory;
        this.passwordChangeLayoutFactory = passwordChangeLayoutFactory;
        setSizeFull();
        setSpacing(false);
        add(getSplitLayout());
    }

    private SplitLayout getSplitLayout() {
        SplitLayout layout = new SplitLayout();
        layout.setSizeFull();
        layout.addToPrimary(getProfileLayout());
        layout.addToSecondary(getPastActivities());
        return layout;
    }

    private VerticalLayout getProfileLayout() {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.add(passwordChangeLayoutFactory.create());
        return layout;
    }

    private Scroller getPastActivities() {
        Scroller scroller = new Scroller(userActivityLayoutFactory.create(), Scroller.ScrollDirection.VERTICAL);
        scroller.setSizeFull();
        return scroller;
    }
}
