package io.overcoded.vaadin;

import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.HasDynamicTitle;
import io.overcoded.grid.MenuLayout;
import io.overcoded.grid.security.GridSecurityService;
import io.overcoded.grid.security.GridUser;
import io.overcoded.grid.util.SecuredMenuLayoutFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class StaticGridAppLayout extends AppLayout implements BeforeEnterObserver {
    private final MenuLayout menuLayout;
    private final StaticNavigationBar navigationBar;
    private final GridSecurityService securityService;
    private final DynamicGridDrawer drawer;
    private final H2 title;

    public StaticGridAppLayout(@Autowired GridSecurityService securityService,
                               @Autowired SecuredMenuLayoutFactory securedMenuLayoutFactory) {
        this.securityService = securityService;
        this.menuLayout = securedMenuLayoutFactory.create();
        this.navigationBar = new StaticNavigationBar();
        this.drawer = new DynamicGridDrawer(menuLayout.getGroups());
        this.title = createTitle(securityService.getDisplayName());
        this.configure(securityService.getAuthenticatedUser());
    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        String path = "/" + beforeEnterEvent.getLocation().getPath();
        drawer.activate(path);
    }

    /**
     * Configures AppLayout
     */
    private void configure(GridUser gridUser) {
        DynamicGridDrawerTitle title = new DynamicGridDrawerTitle(menuLayout, gridUser);
        HorizontalLayout drawerHolder = new HorizontalLayout(drawer);
        drawerHolder.setPadding(true);

        addToDrawer(title, drawerHolder);
        addToNavbar(navigationBar);

        setPrimarySection(Section.DRAWER);
        setDrawerOpened(true);
    }

    @Override
    public void showRouterLayoutContent(HasElement content) {
        super.showRouterLayoutContent(content);
        if (content instanceof MeView) {
            navigationBar.changeContent(getLogoutLayout());
        } else if (content instanceof HasDynamicTitle dynamicTitle) {
            title.setText(dynamicTitle.getPageTitle());
            navigationBar.changeContent(getTitleLayout());
        }
    }

    private HorizontalLayout getTitleLayout() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setWidthFull();
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        layout.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        layout.add(title);
        layout.getElement().getStyle().set("padding-right", "1rem");
        return layout;
    }

    private HorizontalLayout getLogoutLayout() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setWidthFull();
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        layout.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        layout.add(title, createButton());
        layout.getElement().getStyle().set("padding-right", "1rem");
        return layout;
    }

    private Button createButton() {
        Button logout = new Button(new Icon(VaadinIcon.EXIT), click -> securityService.logout());
        logout.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_CONTRAST, ButtonVariant.LUMO_ICON);
        logout.getElement().setAttribute("aria-label", "Logout");
        return logout;
    }

    private H2 createTitle(String displayName) {
        H2 title = new H2(displayName);
        title.getStyle()
                .set("font-size", "var(--lumo-font-size-l)")
                .set("margin-right", "8px")
                .set("margin", "0");
        return title;
    }
}
