package io.overcoded.vaadin;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StaticNavigationBar extends HorizontalLayout {
    private final Div holder = new Div();

    public StaticNavigationBar() {
        configure();
    }

    private void configure() {
        holder.setWidthFull();
        DrawerToggle drawerToggle = new DrawerToggle();
        add(drawerToggle, holder);
        setAlignItems(Alignment.CENTER);
        setPadding(false);
        setSpacing(true);
        setWidthFull();
    }

    public void changeContent(Component component) {
        holder.removeAll();
        holder.add(component);
    }
}
