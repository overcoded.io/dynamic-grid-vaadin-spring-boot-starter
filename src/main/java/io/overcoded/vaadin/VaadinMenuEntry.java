package io.overcoded.vaadin;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.tabs.Tab;
import io.overcoded.grid.MenuEntry;

/**
 * Represents a navigation entry, which can be clicked
 */
public class VaadinMenuEntry extends Tab {
    private final MenuEntry menuEntry;

    public VaadinMenuEntry(MenuEntry menuEntry) {
        this.menuEntry = menuEntry;
        add(createIcon(), createAnchor());
        setSelected(false);
    }

    public String getPath() {
        return menuEntry.getPath();
    }

    private Icon createIcon() {
        return VaadinIcon.valueOf(menuEntry.getIcon()).create();
    }

    private Anchor createAnchor() {
        return new Anchor(menuEntry.getPath(), createLabel());
    }

    private Text createLabel() {
        return new Text(menuEntry.getLabel());
    }
}
