package io.overcoded.vaadin;

import com.vaadin.flow.component.tabs.Tabs;
import io.overcoded.grid.MenuEntry;
import io.overcoded.grid.MenuGroup;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toMap;

public class VaadinMenuGroup extends Tabs {
    private final Map<String, VaadinMenuEntry> menuEntries;

    public VaadinMenuGroup(MenuGroup menuGroup) {
        this.menuEntries = createMenuEntries(menuGroup);
        addMenuEntries();
        configure();
    }

    /**
     * Set tab as active if it's available in this group.
     *
     * @param path
     * @return
     */
    public boolean activate(String path) {
        Optional<String> matchingPath = menuEntries.keySet().stream().filter(path::startsWith).findFirst();
        if (matchingPath.isPresent()) {
            setSelectedTab(menuEntries.get(matchingPath.get()));
        } else {
            menuEntries.values().forEach(menuEntry -> menuEntry.setSelected(false));
            setSelectedTab(null);
        }
        return matchingPath.isPresent();
    }

    /**
     * Create menu entries map, keeping the original ordering of menu entries
     * in menu group.
     *
     * @param menuGroup from where the menu entries should be extracted
     * @return a map which should hold the insertion order
     */
    private Map<String, VaadinMenuEntry> createMenuEntries(MenuGroup menuGroup) {
        return menuGroup.getEntries()
                .stream()
                .collect(toMap(MenuEntry::getPath, VaadinMenuEntry::new, (firstEntry, secondEntry) -> firstEntry, LinkedHashMap::new));
    }

    /**
     * Add menu entries to the group in order.
     */
    private void addMenuEntries() {
        menuEntries.forEach((s, menuEntry) -> super.add(menuEntry));
    }

    /**
     * Configures Tabs Vaadin component
     */
    private void configure() {
        setSelectedIndex(-1);
        setAutoselect(false);
        setOrientation(Orientation.VERTICAL);
    }
}
