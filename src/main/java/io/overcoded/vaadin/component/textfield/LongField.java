package io.overcoded.vaadin.component.textfield;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.textfield.AbstractNumberField;
import com.vaadin.flow.function.SerializableFunction;

@Tag("overcoded-long-field")
public class LongField extends AbstractNumberField<LongField, Long> {
    private static final SerializableFunction<String, Long> PARSER = (valueFormClient) -> {
        if (valueFormClient != null && !valueFormClient.isEmpty()) {
            try {
                return Long.parseLong(valueFormClient);
            } catch (NumberFormatException var2) {
                return null;
            }
        } else {
            return null;
        }
    };
    private static final SerializableFunction<Long, String> FORMATTER = (valueFromModel) -> {
        return valueFromModel == null ? "" : valueFromModel.toString();
    };

    public LongField() {
        super(PARSER, FORMATTER, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    public LongField(String label) {
        this();
        this.setLabel(label);
    }

    public LongField(String label, String placeholder) {
        this(label);
        this.setPlaceholder(placeholder);
    }

    public LongField(HasValue.ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<LongField, Long>> listener) {
        this();
        this.addValueChangeListener(listener);
    }

    public LongField(String label, HasValue.ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<LongField, Long>> listener) {
        this(label);
        this.addValueChangeListener(listener);
    }

    public LongField(String label, Long initialValue, HasValue.ValueChangeListener<? super AbstractField.ComponentValueChangeEvent<LongField, Long>> listener) {
        this(label);
        this.setValue(initialValue);
        this.addValueChangeListener(listener);
    }


    public void setMin(long min) {
        super.setMin((double) min);
    }

    public long getMin() {
        return (long) this.getMinDouble();
    }

    public void setMax(int max) {
        super.setMax((double) max);
    }

    public long getMax() {
        return (int) this.getMaxDouble();
    }

    public void setStep(int step) {
        if (step <= 0) {
            throw new IllegalArgumentException("The step cannot be less or equal to zero.");
        } else {
            super.setStep((double) step);
        }
    }

    public long getStep() {
        return (long) this.getStepDouble();
    }

}
