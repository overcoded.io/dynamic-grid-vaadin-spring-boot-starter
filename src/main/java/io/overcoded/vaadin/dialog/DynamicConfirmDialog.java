package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;

public class DynamicConfirmDialog extends ConfirmDialog {
    public DynamicConfirmDialog(String confirmText, String theme, Runnable callback) {
        setRejectable(true);
        setRejectButton(new Button("Cancel", e -> close()));
        setConfirmText(confirmText);
        setConfirmButtonTheme(theme);
        setHeader("Continue: " + confirmText);
        setText("Are you sure you want to continue?");
        addConfirmListener(e -> callback.run());
    }
}
