package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.dialog.DialogVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import io.overcoded.grid.DialogProperties;
import io.overcoded.grid.GridInfo;

import java.util.Objects;

public abstract class DynamicDialog<T, U> extends Dialog implements HasUrlParameter<DynamicDialogParameter<T, U>> {
    private final DialogProperties properties;
    protected final GridInfo gridInfo;

    public DynamicDialog(DialogProperties dialogProperties, GridInfo gridInfo) {
        this.properties = dialogProperties;
        this.gridInfo = gridInfo;
        this.configure(gridInfo.getName(), gridInfo.getDescription());
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, DynamicDialogParameter<T, U> parameter) {
        add(createContent(parameter));
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        setVisible(true);
    }

    private void configure(String title, String description) {
        if (properties.isFullSized()) {
            setSizeFull();
        }
        if (!properties.isWithPadding()) {
            addThemeVariants(DialogVariant.LUMO_NO_PADDING);
        }
        setModal(properties.isModal());
        setResizable(properties.isResizable());
        setDraggable(properties.isDraggable());
        setCloseOnEsc(properties.isCloseOnEscape());
        setHeader(title, description);
        setCloseOnOutsideClick(properties.isCloseOnOutsideClick());
    }

    private void setHeader(String title, String description) {
        setHeaderTitle(title);
        setDescriptionIfPresent(description);
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void setDescriptionIfPresent(String description) {
        if (Objects.nonNull(description) && !description.isBlank()) {
            getHeader().add(createDescription(description));
        }
    }

    protected abstract Component createContent(DynamicDialogParameter<T, U> parameter);

    private Span createDescription(String text) {
        Span description = new Span(createIcon(), new Span(text));
        description.getElement().getThemeList().add("badge primary");
        return description;
    }

    private Icon createIcon() {
        Icon icon = VaadinIcon.HAND.create();
        icon.getStyle().set("padding", "var(--lumo-space-xs)");
        return icon;
    }
}
