package io.overcoded.vaadin.dialog;

import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.DialogInfo;
import io.overcoded.grid.DialogProperties;
import io.overcoded.grid.DialogType;
import io.overcoded.grid.GridInfo;
import io.overcoded.grid.processor.DialogInfoFactory;
import io.overcoded.grid.processor.FieldCollector;
import io.overcoded.vaadin.DynamicRelationshipService;
import io.overcoded.vaadin.GridCrudFactory;
import io.overcoded.vaadin.panel.DynamicGridPanelFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.util.function.BiConsumer;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class DynamicDialogFactory {
    private final FieldCollector fieldCollector;
    private final GridCrudFactory gridCrudFactory;
    private final DialogProperties dialogProperties;
    private final DialogInfoFactory dialogInfoFactory;
    private final DynamicGridPanelFactory panelFactory;
    private final DynamicRelationshipService dynamicRelationshipService;

    /**
     * @param <T> type of the dialog
     * @param <U> filter type of the dialog
     * @return
     */
    public <T, U> DynamicDialog<T, U> create(GridCrud<U> gridCrud, DynamicDialogParameter<T, U> parameter, BiConsumer<GridCrud<T>, GridInfo> configurer) {
        DialogInfo dialogInfo = dialogInfoFactory.create(parameter.getType(), parameter.getParameter().getClass());
        DialogType dialogType = dialogInfo.getRenderingType();
        parameter.setDialogType(dialogType);
        DynamicDialog<T, U> dynamicDialog;
        if (dialogType == DialogType.MANY_TO_MANY_DIALOG) {
            dynamicDialog = new DynamicManyToManyDialog<>(dynamicRelationshipService, gridCrudFactory, dialogProperties, dialogInfo.getGridInfo());
        } else if (dialogType == DialogType.ONE_TO_ONE_DIALOG) {
            dynamicDialog = new DynamicOneToOneDialog<>(fieldCollector, gridCrudFactory, dialogProperties, configurer, dialogInfo.getGridInfo());
            dynamicDialog.addOpenedChangeListener(e -> {
                if (!e.isOpened()) {
                    gridCrud.refreshGrid();
                }
            });
        } else if (dialogType == DialogType.ONE_TO_MANY_DIALOG || dialogType == DialogType.REVERSE_MANY_TO_ONE_DIALOG) {
            dynamicDialog = new DynamicOneToManyDialog<>(gridCrudFactory, dialogProperties, configurer, dialogInfo.getGridInfo());
        } else {
            dynamicDialog = new DynamicUnknownDialog<>(panelFactory, dialogProperties, dialogInfo.getGridInfo());
        }
        dynamicDialog.setParameter(null, parameter);
        return dynamicDialog;
    }


}
