package io.overcoded.vaadin.dialog;

import io.overcoded.grid.DialogGridMenuItem;
import io.overcoded.grid.DialogType;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @param <T> type of the dialog
 * @param <U> type of the parameter (filter column) should be present as field in T
 */
@Data
@Builder
public class DynamicDialogParameter<T, U> {
    private Class<T> type;
    private String property;
    private DialogGridMenuItem entry;
    private U parameter;
    private Class<U> parameterType;
    private DialogType dialogType;
    /**
     * In case of many-to-many relationship, this stores the name
     * of the field in the owner side
     */
    private String reverseProperty;
    /**
     * In case of many-to-many relationship, this stores the already
     * owned elements
     */
    private List<T> ownedEntries;
}
