package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Paragraph;

public class DynamicLinkDialog extends Dialog {
    public DynamicLinkDialog(String finalUrl) {
        setWidthFull();
        setCloseOnOutsideClick(false);
        setCloseOnEsc(true);
        setModal(true);

        add(new Paragraph(finalUrl));
    }
}
