package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.grid.dnd.GridDragEndEvent;
import com.vaadin.flow.component.grid.dnd.GridDragStartEvent;
import com.vaadin.flow.component.grid.dnd.GridDropMode;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.theme.lumo.LumoUtility;
import io.overcoded.grid.DialogProperties;
import io.overcoded.grid.GridInfo;
import io.overcoded.vaadin.DynamicRelationshipService;
import io.overcoded.vaadin.GridCrudFactory;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
public class DynamicManyToManyDialog<T, U> extends DynamicDialog<T, U> {
    private final DynamicRelationshipService dynamicRelationshipService;
    private final GridCrudFactory gridCrudFactory;
    private T draggedItem;

    public DynamicManyToManyDialog(DynamicRelationshipService dynamicRelationshipService,
                                   GridCrudFactory gridCrudFactory,
                                   DialogProperties dialogProperties,
                                   GridInfo gridInfo) {
        super(dialogProperties, gridInfo);
        this.dynamicRelationshipService = dynamicRelationshipService;
        this.gridCrudFactory = gridCrudFactory;
    }

    @Override
    protected Component createContent(DynamicDialogParameter<T, U> parameter) {
        return createLayout(parameter.getParameter(), parameter.getReverseProperty(), parameter.getOwnedEntries(), parameter.getType());
    }

    private HorizontalLayout createLayout(U owner, String property, List<T> owned, Class<T> type) {
        log.info("{} owned {} {}", owner, type, owned);
        Set<T> originallyOwned = new HashSet<>(owned);

        GridCrud<T> ownedItemsGridCrud = gridCrudFactory.create(withDisabledForm(gridInfo));
        configureGridCrud(ownedItemsGridCrud);
        ownedItemsGridCrud.setFindAllOperation(new ListDataProvider<>(owned));
        Grid<T> ownedItems = ownedItemsGridCrud.getGrid();
        ownedItems.setClassNameGenerator(item -> {
            String className = null;
            if (!originallyOwned.contains(item)) {
                className = LumoUtility.TextColor.SUCCESS + " " + LumoUtility.Background.SUCCESS_10;
            }
            return className;
        });
        GridListDataView<T> view = ownedItems.setItems(owned);

        GridCrud<T> availableItemsGridCrud = gridCrudFactory.create(withDisabledForm(gridInfo));
        configureGridCrud(availableItemsGridCrud);
        Grid<T> availableItems = availableItemsGridCrud.getGrid();
        availableItems.setDragFilter(item -> !view.contains(item));
        availableItems.setClassNameGenerator(item -> {
            String className = null;
            if (view.contains(item)) {
                className = LumoUtility.TextColor.DISABLED;
            } else if (originallyOwned.contains(item)) {
                className = LumoUtility.TextColor.ERROR + " " + LumoUtility.Background.ERROR_10;
            }
            return className;
        });

        ownedItems.addDropListener(event -> {
            view.addItem(draggedItem);
            availableItems.getDataProvider().refreshItem(draggedItem);
        });

        availableItems.addDropListener(event -> {
            view.removeItem(draggedItem);
            availableItems.getDataProvider().refreshItem(draggedItem);
        });

        configureFooter(owner, property, view);

        return new HorizontalLayout(availableItemsGridCrud, wrapOwnedItems(ownedItems));
    }

    private VerticalLayout wrapOwnedItems(Grid<T> grid) {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setPadding(false);
        verticalLayout.setMargin(false);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setPadding(false);

        HorizontalLayout beforeToolbar = new HorizontalLayout();
        beforeToolbar.setPadding(false);
        beforeToolbar.setMargin(false);

        HorizontalLayout toolbar = new HorizontalLayout();
        toolbar.setPadding(false);
        toolbar.setMargin(false);
        Button button = new Button(" ");
        button.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        button.setEnabled(false);
        toolbar.add(button);

        VerticalLayout gridHolder = new VerticalLayout();
        gridHolder.setPadding(false);
        gridHolder.setMargin(true);
        gridHolder.add(grid);


        horizontalLayout.add(beforeToolbar, toolbar);
        verticalLayout.add(horizontalLayout, gridHolder);

        return verticalLayout;
    }

    private void configureGridCrud(GridCrud<T> gridCrud) {
        gridCrud.setDeleteOperationVisible(false);
        gridCrud.setDeleteOperation(item -> {
        });
        gridCrud.setUpdateOperationVisible(false);
        gridCrud.setUpdateOperation(item -> item);
        gridCrud.setAddOperationVisible(false);
        gridCrud.setAddOperation(item -> item);
        gridCrud.setFindAllOperationVisible(false);

        Grid<T> grid = gridCrud.getGrid();
        grid.setDropMode(GridDropMode.ON_GRID);
        grid.setRowsDraggable(true);
        grid.addDragStartListener(this::handleDragStart);
        grid.addDragEndListener(this::handleDragEnd);
    }

    private void handleDragStart(GridDragStartEvent<T> e) {
        draggedItem = e.getDraggedItems().get(0);
    }

    private void handleDragEnd(GridDragEndEvent<T> e) {
        draggedItem = null;
    }

    private GridInfo withDisabledForm(GridInfo gridInfo) {
        return GridInfo.builder()
                .formEnabled(false)
                .type(gridInfo.getType())
                .name(gridInfo.getName())
                .columns(gridInfo.getColumns())
                .enabledFor(gridInfo.getEnabledFor())
                .menuGroups(gridInfo.getMenuGroups())
                .description(gridInfo.getDescription())
                .enabledMethods(gridInfo.getEnabledMethods())
                .sourceAnnotation(gridInfo.getSourceAnnotation())
                .build();
    }

    private void configureFooter(U owner, String property, GridListDataView<T> view) {
        Button deleteButton = new Button("Save changes", event -> {
            List<T> items = view.getItems().toList();
            log.debug("Saving {} of {}, with {}", property, owner, items);
            dynamicRelationshipService.save(owner, items, property);
            close();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        deleteButton.getStyle().set("margin-right", "auto");
        getFooter().add(deleteButton);

        Button cancelButton = new Button("Cancel", event -> close());
        cancelButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getFooter().add(cancelButton);
    }
}
