package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.Component;
import io.overcoded.grid.DialogProperties;
import io.overcoded.grid.GridInfo;
import io.overcoded.vaadin.GridCrudFactory;
import io.overcoded.vaadin.grid.DynamicGridCrudCallback;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.util.function.BiConsumer;

public class DynamicOneToManyDialog<T, U> extends DynamicDialog<T, U> {
    private final GridCrudFactory gridCrudFactory;
    private final BiConsumer<GridCrud<T>, GridInfo> configurer;

    public DynamicOneToManyDialog(GridCrudFactory gridCrudFactory,
                                  DialogProperties dialogProperties,
                                  BiConsumer<GridCrud<T>, GridInfo> configurer,
                                  GridInfo gridInfo) {
        super(dialogProperties, gridInfo);
        this.gridCrudFactory = gridCrudFactory;
        this.configurer = configurer;
    }

    @Override
    protected Component createContent(DynamicDialogParameter<T, U> parameter) {
        return getGridCrud(parameter, null);
    }

    protected GridCrud<T> getGridCrud(DynamicDialogParameter<T, U> parameter, DynamicGridCrudCallback<T> callback) {
        GridCrud<T> gridCrud = gridCrudFactory.create(gridInfo, parameter, callback);
        configurer.accept(gridCrud, gridInfo);
        return gridCrud;
    }
}
