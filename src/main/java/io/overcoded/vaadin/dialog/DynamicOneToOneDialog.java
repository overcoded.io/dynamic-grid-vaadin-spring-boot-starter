package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import io.overcoded.grid.DialogProperties;
import io.overcoded.grid.GridInfo;
import io.overcoded.grid.processor.FieldCollector;
import io.overcoded.vaadin.GridCrudFactory;
import io.overcoded.vaadin.grid.DynamicGridCrudCallback;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;

@Slf4j
public class DynamicOneToOneDialog<T, U> extends DynamicOneToManyDialog<T, U> {
    private final FieldCollector fieldCollector;
    private GridCrud<T> gridCrud;

    private T instance;

    public DynamicOneToOneDialog(FieldCollector fieldCollector, GridCrudFactory gridCrudFactory, DialogProperties dialogProperties, BiConsumer<GridCrud<T>, GridInfo> configurer, GridInfo gridInfo) {
        super(gridCrudFactory, dialogProperties, configurer, gridInfo);
        this.fieldCollector = fieldCollector;
    }

    @Override
    protected Component createContent(DynamicDialogParameter<T, U> parameter) {
        String property = parameter.getEntry().isCustomFilter()
                ? parameter.getEntry().getParentFieldName()
                : parameter.getProperty();
        Optional<T> theOne = getInstance(parameter.getParameter(), property, parameter.getType());
        gridCrud = getGridCrud(parameter, new DynamicGridCrudCallback<>() {
            @Override
            public void afterAdd(T t) {
                close();
            }
        });
        gridCrud.setFindAllOperation(() -> theOne.map(List::of).orElseGet(List::of));
        gridCrud.getAddButton().setDisableOnClick(true);
        gridCrud.setFindAllOperationVisible(false);
        if (theOne.isPresent()) {
            instance = theOne.get();
            gridCrud.setAddOperationVisible(false);
        } else {
            gridCrud.getAddButton().setDisableOnClick(true);
        }
        return gridCrud;
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        if (Objects.nonNull(instance)) {
            gridCrud.getGrid().select(instance);
        }
    }

    private Optional<T> getInstance(U parentInstance, String property, Class<?> type) {
        Class<?> parentType = parentInstance.getClass();
        Optional<T> instance = Optional.empty();
        try {
            Optional<Field> childField = fieldCollector.getFields(parentType)
                    .stream()
                    .filter(field -> field.getName().equals(property) || field.getType().equals(type))
                    .findFirst();
            if (childField.isPresent()) {
                Field field = childField.get();
                field.setAccessible(true);
                instance = Optional.ofNullable((T) field.get(parentInstance));
                field.setAccessible(false);
            }
        } catch (Exception ex) {
            // TODO
        }
        return instance;
    }
}
