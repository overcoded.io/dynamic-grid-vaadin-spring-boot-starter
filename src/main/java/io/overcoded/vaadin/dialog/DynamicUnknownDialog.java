package io.overcoded.vaadin.dialog;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Paragraph;
import io.overcoded.grid.DialogProperties;
import io.overcoded.grid.GridInfo;
import io.overcoded.vaadin.panel.DynamicGridPanelFactory;

public class DynamicUnknownDialog<T, U> extends DynamicDialog<T, U> {
    private final DynamicGridPanelFactory panelFactory;

    public DynamicUnknownDialog(DynamicGridPanelFactory panelFactory, DialogProperties dialogProperties, GridInfo gridInfo) {
        super(dialogProperties, gridInfo);
        this.panelFactory = panelFactory;
    }

    @Override
    protected Component createContent(DynamicDialogParameter<T, U> parameter) {
        return panelFactory.createError("Unknown dialog type!", new Paragraph("Something went wrong or this is not a supported dialog type."));
    }
}
