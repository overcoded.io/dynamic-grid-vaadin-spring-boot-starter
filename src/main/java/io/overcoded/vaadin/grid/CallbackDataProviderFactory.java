package io.overcoded.vaadin.grid;

import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import lombok.RequiredArgsConstructor;

@SpringComponent
@RequiredArgsConstructor
public class CallbackDataProviderFactory {

    public <T> CallbackDataProvider<T, Object> create(GridFilterRepository<T> gridFilterRepository) {
        CallbackDataProvider.FetchCallback<T, Object> fetchCallback = new DynamicFetchCallback<>(gridFilterRepository);
        CallbackDataProvider.CountCallback<T, Object> countCallback = new DynamicCountCallback<>(gridFilterRepository);
        return new CallbackDataProvider<>(fetchCallback, countCallback);
    }
}
