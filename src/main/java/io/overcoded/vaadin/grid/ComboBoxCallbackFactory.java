package io.overcoded.vaadin.grid;

import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

@SpringComponent
public class ComboBoxCallbackFactory {
    public <T> CallbackDataProvider.FetchCallback<Object, String> getFetchItemsCallback(ComboBoxFilterRepository<T> filterRepository, JpaRepository<T, Long> jpaRepository) {
        return (query) -> query.getFilter().isEmpty() || query.getFilter().get().isBlank()
                ? jpaRepository.findAll(PageRequest.of(query.getPage(), query.getLimit())).stream().map(t -> (Object) t)
                : filterRepository.findAllMatching((Query<T, String>) query).stream().map(t -> (Object) t);
    }

    public <T> CallbackDataProvider.FetchCallback<Object, String> getFetchItemsCallback(JpaRepository<T, Long> jpaRepository) {
        return (query) -> jpaRepository.findAll(PageRequest.of(query.getPage(), query.getLimit()))
                .stream()
                .map(t -> (Object) t);
    }
}
