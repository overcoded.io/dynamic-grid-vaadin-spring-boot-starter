package io.overcoded.vaadin.grid;

import com.vaadin.flow.data.provider.Query;
import io.overcoded.grid.ColumnInfo;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
public class ComboBoxFilterRepository<T> {
    private final EntityManager entityManager;
    private final ColumnInfo mainFilter;
    private final Class<T> type;

    public long countAllMatching() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> root = criteriaQuery.from(type);

        criteriaQuery.select(criteriaBuilder.count(root));

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    public long countAllMatching(Query<T, String> query) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<T> root = criteriaQuery.from(type);
        Optional<Predicate> predicate = query.getFilter().flatMap(filter -> getPredicate(criteriaBuilder, root, filter));

        criteriaQuery.select(criteriaBuilder.count(root));
        predicate.ifPresent(criteriaQuery::where);

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    public List<T> findAllMatching(Query<T, String> query) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(type);

        Root<T> root = criteriaQuery.from(type);

        Optional<Predicate> predicate = query.getFilter().flatMap(filter -> getPredicate(criteriaBuilder, root, filter));

        criteriaQuery.select(root);
        predicate.ifPresent(criteriaQuery::where);

        return entityManager
                .createQuery(criteriaQuery)
                .setFirstResult(query.getOffset())
                .setMaxResults(query.getLimit())
                .getResultList();
    }

    private Optional<Predicate> getPredicate(CriteriaBuilder criteriaBuilder, Root<T> root, String value) {
        Optional<Predicate> predicate = Optional.empty();
        String fieldName = mainFilter.getName();
        if (Objects.nonNull(fieldName) && Objects.nonNull(value) && !value.isBlank()) {
            predicate = Optional.of(criteriaBuilder.like(criteriaBuilder.upper(root.get(fieldName)), "%" + value.toUpperCase() + "%"));
        }
        return predicate;
    }
}