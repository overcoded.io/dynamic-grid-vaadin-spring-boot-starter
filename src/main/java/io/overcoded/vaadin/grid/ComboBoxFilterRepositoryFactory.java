package io.overcoded.vaadin.grid;

import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.processor.ColumnInfoCollector;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@SpringComponent
@RequiredArgsConstructor
public class ComboBoxFilterRepositoryFactory {
    private final ColumnInfoCollector columnInfoCollector;
    private final EntityManager entityManager;

    public <T> Optional<ComboBoxFilterRepository<T>> create(Class<T> type) {
        List<ColumnInfo> columns = columnInfoCollector.collect(type);
        return getMainFilter(columns)
                .or(() -> getFirstStringFilter(columns))
                .or(() -> getFirstString(columns))
                .map(mainFilter -> new ComboBoxFilterRepository<>(entityManager, mainFilter, type));
    }

    private Optional<ColumnInfo> getMainFilter(List<ColumnInfo> columns) {
        return columns.stream().filter(ColumnInfo::isMainFilter).findFirst();
    }

    private Optional<ColumnInfo> getFirstStringFilter(List<ColumnInfo> columns) {
        return columns.stream()
                .filter(ColumnInfo::isFilter)
                .filter(columnInfo -> columnInfo.getType().isAssignableFrom(String.class))
                .findFirst();
    }

    private Optional<ColumnInfo> getFirstString(List<ColumnInfo> columns) {
        return columns.stream()
                .filter(columnInfo -> columnInfo.getType().isAssignableFrom(String.class))
                .findFirst();
    }
}
