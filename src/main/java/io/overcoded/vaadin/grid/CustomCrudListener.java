package io.overcoded.vaadin.grid;

import com.vaadin.flow.component.AbstractField;
import org.vaadin.crudui.crud.CrudListener;

import java.util.List;

/**
 * CustomCrudListener serves dynamic grids which are not entity / jpa repository based grids
 * Please mark the implementations as prototype beans.
 */
public interface CustomCrudListener<T> extends CrudListener<T> {
    Class<T> handledType();

    void setFilterComponents(List<AbstractField<?, ?>> filterComponents);
}
