package io.overcoded.vaadin.grid;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.ResolvableType;
import org.vaadin.crudui.crud.CrudListener;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@SpringComponent
@RequiredArgsConstructor
public class CustomCrudListenerFactory {
    private final BeanFactory beanFactory;

    public <T> Optional<CrudListener<T>> findCrudListener(Class<T> type, List<AbstractField<?, ?>> filterComponents) {
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(CustomCrudListener.class, type);
        ObjectProvider<CustomCrudListener<T>> beanProvider = beanFactory.getBeanProvider(resolvableType);
        CustomCrudListener<T> customCrudListener = beanProvider.getIfAvailable();
        if (Objects.nonNull(customCrudListener)) {
            customCrudListener.setFilterComponents(filterComponents);
        }
        return Optional.ofNullable(customCrudListener);
    }
}
