package io.overcoded.vaadin.grid;

import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.Query;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class DynamicCountCallback<T, F> implements CallbackDataProvider.CountCallback<T, F> {
    private final GridFilterRepository<T> gridFilterRepository;

    @Override
    public int count(Query<T, F> query) {
        return (int) gridFilterRepository.countAllMatching();
    }
}
