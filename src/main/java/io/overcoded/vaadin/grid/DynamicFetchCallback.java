package io.overcoded.vaadin.grid;

import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.Query;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.stream.Stream;

@Slf4j
@RequiredArgsConstructor
public class DynamicFetchCallback<T, F> implements CallbackDataProvider.FetchCallback<T, F> {
    private final GridFilterRepository<T> methodInfoInvoker;

    @Override
    public Stream<T> fetch(Query<T, F> query) {
        return methodInfoInvoker.findAllMatching(query).stream();
    }
}
