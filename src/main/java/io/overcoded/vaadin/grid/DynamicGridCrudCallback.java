package io.overcoded.vaadin.grid;

public interface DynamicGridCrudCallback<T> {
    default void afterAdd(T t) {
    }

    default void afterUpdate(T t) {
    }

    default void afterDelete(T t) {
    }
}
