package io.overcoded.vaadin.grid;

import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.DataProvider;
import io.overcoded.grid.security.GridUserActivityService;
import io.overcoded.grid.util.EntityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.vaadin.crudui.crud.LazyCrudListener;

import java.util.Objects;
import java.util.function.BiConsumer;

@RequiredArgsConstructor
public class DynamicGridCrudListener<T> implements LazyCrudListener<T> {
    private final EntityUtil entityUtil;
    protected final JpaRepository<T, Long> jpaRepository;
    private final GridUserActivityService userActivityService;
    private final DynamicGridCrudCallback<T> gridCrudCallback;
    private final CallbackDataProvider<T, Object> callbackDataProvider;

    @Override
    public DataProvider<T, ?> getDataProvider() {
        return callbackDataProvider;
    }

    @Override
    @Transactional
    public T add(T t) {
        T savedEntity = jpaRepository.save(t);
        userActivityService.logCreate(savedEntity, entityUtil.extractId(savedEntity));
        runIfPresent(DynamicGridCrudCallback::afterAdd, savedEntity);
        return savedEntity;
    }

    @Override
    @Transactional
    public T update(T t) {
        userActivityService.logUpdate(t, entityUtil.extractId(t), jpaRepository::findById);
        T updatedEntity = jpaRepository.save(t);
        runIfPresent(DynamicGridCrudCallback::afterUpdate, updatedEntity);
        return updatedEntity;
    }

    @Override
    @Transactional
    public void delete(T t) {
        userActivityService.logDelete(t, entityUtil.extractId(t));
        jpaRepository.delete(t);
        runIfPresent(DynamicGridCrudCallback::afterDelete, t);
    }

    private void runIfPresent(BiConsumer<DynamicGridCrudCallback<T>, T> supplier, T instance) {
        if (Objects.nonNull(gridCrudCallback)) {
            supplier.accept(gridCrudCallback, instance);
        }
    }
}
