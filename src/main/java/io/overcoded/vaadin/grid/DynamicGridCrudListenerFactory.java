package io.overcoded.vaadin.grid;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.GridInfo;
import io.overcoded.grid.security.GridSecurityRepositories;
import io.overcoded.grid.security.GridUserActivityService;
import io.overcoded.grid.security.GridUserDetailsManager;
import io.overcoded.grid.util.EntityUtil;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.support.Repositories;
import org.vaadin.crudui.crud.CrudListener;

import java.util.List;
import java.util.Optional;

@SpringComponent
@RequiredArgsConstructor
public class DynamicGridCrudListenerFactory {
    private final CallbackDataProviderFactory callbackDataProviderFactory;
    private final EntityUtil entityUtil;
    private final Repositories repositories;
    private final EntityManager entityManager;
    private final GridUserActivityService userActivityService;
    private final GridSecurityRepositories securityRepositories;
    private final GridUserDetailsManager gridUserDetailsManager;
    private final GridFilterRepositoryFactory gridFilterRepositoryFactory;

    public <T> Optional<CrudListener<T>> create(GridInfo gridInfo, List<AbstractField<?, ?>> filterComponents, DynamicGridCrudCallback<T> gridCrudCallback) {
        @SuppressWarnings("unchecked")
        Class<T> type = (Class<T>) gridInfo.getType();
        @SuppressWarnings("unchecked")
        Optional<JpaRepository<T, Long>> repositoryFor = repositories.getRepositoryFor(type).map(o -> (JpaRepository<T, Long>) o);
        return repositoryFor
                .map(jpaRepository -> toCrudListener(filterComponents, gridCrudCallback, type, jpaRepository));
    }

    private <T> CrudListener<T> toCrudListener(List<AbstractField<?, ?>> filterComponents, DynamicGridCrudCallback<T> gridCrudCallback, Class<T> type, JpaRepository<T, Long> jpaRepository) {
        return isUserRepository(jpaRepository) ? getDynamicUserGridCrudListener(type, filterComponents) : getDynamicGridCrudListener(jpaRepository, filterComponents, type, gridCrudCallback);
    }

    private <T> DynamicGridCrudListener<T> getDynamicGridCrudListener(JpaRepository<T, Long> jpaRepository, List<AbstractField<?, ?>> filterComponents, Class<T> type, DynamicGridCrudCallback<T> gridCrudCallback) {
        GridFilterRepository<T> gridFilterRepository = gridFilterRepositoryFactory.create(entityManager, filterComponents, type);
        CallbackDataProvider<T, Object> dataProvider = callbackDataProviderFactory.create(gridFilterRepository);
        return new DynamicGridCrudListener<>(entityUtil, jpaRepository, userActivityService, gridCrudCallback, dataProvider);
    }

    private <T> CrudListener<T> getDynamicUserGridCrudListener(Class<T> type, List<AbstractField<?, ?>> filterComponents) {
        return new DynamicUserGridCrudListener<>(type, entityManager, filterComponents, gridUserDetailsManager);
    }

    private <T> boolean isUserRepository(JpaRepository<T, Long> jpaRepository) {
        return securityRepositories.getGridUserRepository()
                .filter(userRepository -> userRepository.equals(jpaRepository))
                .isPresent();
    }

}
