package io.overcoded.vaadin.grid;

import com.vaadin.flow.component.ItemLabelGenerator;
import com.vaadin.flow.function.ValueProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.support.StandardEvaluationContext;

@Slf4j
@RequiredArgsConstructor
public class DynamicItemLabelGenerator<T> implements ItemLabelGenerator<T>, ValueProvider<T, String> {
    private final Expression expression;

    @Override
    public String apply(T item) {
        String result = String.valueOf(item);
        try {
            EvaluationContext context = new StandardEvaluationContext(item);
            result = (String) expression.getValue(context);
        } catch (EvaluationException ex) {
            log.trace("SpelExpression apply failed. {}", ex.getMessage());
        }
        return result;
    }
}
