package io.overcoded.vaadin.grid;

import lombok.RequiredArgsConstructor;
import org.springframework.expression.ExpressionParser;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.joining;

@Component
@RequiredArgsConstructor
public class DynamicItemLabelGeneratorFactory {
    private final ExpressionParser expressionParser;

    public <T> DynamicItemLabelGenerator<T> create(List<String> displayValueExpressionParts) {
        return create("", displayValueExpressionParts);
    }

    public <T> DynamicItemLabelGenerator<T> create(String columnName, List<String> displayValueExpressionParts) {
        String expression = getExpression(columnName, displayValueExpressionParts);
        return new DynamicItemLabelGenerator<>(expressionParser.parseExpression(expression));
    }

    private String getExpression(String columnName, List<String> displayValueExpressionParts) {
        String prefix = Objects.isNull(columnName) || columnName.isBlank() ? "" : columnName + ".";
        return displayValueExpressionParts
                .stream()
                .map(part -> part.startsWith("'") ? part : prefix + part)
                .collect(joining(" + "));
    }
}
