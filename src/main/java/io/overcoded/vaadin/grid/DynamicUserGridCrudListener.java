
package io.overcoded.vaadin.grid;

import com.vaadin.flow.component.AbstractField;
import io.overcoded.grid.security.GridUser;
import io.overcoded.grid.security.GridUserDetailsManager;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.vaadin.crudui.crud.CrudListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
public class DynamicUserGridCrudListener<T> implements CrudListener<T> {
    private final Class<T> type;
    private final EntityManager entityManager;
    private final List<AbstractField<?, ?>> filterComponents;
    private final GridUserDetailsManager gridUserDetailsManager;

    @Override
    public Collection<T> findAll() {
        List<Predicate> predicates = new ArrayList<>();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(type);
        Root<T> root = criteriaQuery.from(type);

        filterComponents.forEach(component -> {
            Object value = component.getValue();
            String fieldName = component.getId().orElse(null);

            if (Objects.nonNull(fieldName) && Objects.nonNull(value)) {
                if (value instanceof String filterText) {
                    predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get(fieldName)), "%" + filterText.toUpperCase() + "%"));
                } else {
                    predicates.add(criteriaBuilder.equal(root.get(fieldName), value));
                }
            }
        });

        if (!predicates.isEmpty()) {
            criteriaQuery.select(root).where(criteriaBuilder.and(predicates.toArray(new Predicate[0])));
        }

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    @Transactional
    public T add(T t) {
        return (T) gridUserDetailsManager.createUser((GridUser) t);
    }

    @Override
    @Transactional
    public T update(T t) {
        return (T) gridUserDetailsManager.updateUser((GridUser) t);
    }

    @Override
    @Transactional
    public void delete(T t) {
        gridUserDetailsManager.deleteUser((GridUser) t);
    }
}
