package io.overcoded.vaadin.grid;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.QuerySortOrder;
import com.vaadin.flow.data.provider.SortDirection;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.*;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
public class GridFilterRepository<T> {
    private final List<AbstractField<?, ?>> filterComponents;
    private final EntityManager entityManager;
    private final Class<T> type;

    public long countAllMatching() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<T> root = criteriaQuery.from(type);
        List<Predicate> predicates = getPredicates(criteriaBuilder, root);

        criteriaQuery.select(criteriaBuilder.count(root));
        if (!predicates.isEmpty()) {
            criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[0])));
        }

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    public List<T> findAllMatching(Query<T, ?> query) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(type);

        Root<T> root = criteriaQuery.from(type);
        List<Predicate> predicates = getPredicates(criteriaBuilder, root);
        List<Order> orders = getOrders(criteriaBuilder, root, query.getSortOrders());

        criteriaQuery.select(root);
        if (!predicates.isEmpty()) {
            criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[0])));
        }

        if (!orders.isEmpty()) {
            criteriaQuery.orderBy(orders);
        }

        return entityManager
                .createQuery(criteriaQuery)
                .setFirstResult(query.getOffset())
                .setMaxResults(query.getLimit())
                .getResultList();
    }

    private List<Order> getOrders(CriteriaBuilder criteriaBuilder, Root<T> root, List<QuerySortOrder> sortOrders) {
        return sortOrders.stream().map(sortOrder -> asOrder(criteriaBuilder, root, sortOrder)).toList();
    }

    private Order asOrder(CriteriaBuilder criteriaBuilder, Root<T> root, QuerySortOrder sortOrder) {
        Path<Object> field = root.get(sortOrder.getSorted());
        return sortOrder.getDirection() == SortDirection.ASCENDING
                ? criteriaBuilder.asc(field)
                : criteriaBuilder.desc(field);
    }

    private List<Predicate> getPredicates(CriteriaBuilder criteriaBuilder, Root<T> root) {
        List<Predicate> predicates = new ArrayList<>();
        filterComponents.forEach(component -> {
            Object value = component.getValue();
            String fieldName = component.getId().orElse(null);

            if (Objects.nonNull(fieldName) && Objects.nonNull(value)) {
                if (value instanceof String filterText) {
                    if (!filterText.isBlank()) {
                        predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get(fieldName)), "%" + filterText.toUpperCase() + "%"));
                    }
                } else {
                    predicates.add(criteriaBuilder.equal(root.get(fieldName), value));
                }
            }
        });
        return predicates;
    }


}
