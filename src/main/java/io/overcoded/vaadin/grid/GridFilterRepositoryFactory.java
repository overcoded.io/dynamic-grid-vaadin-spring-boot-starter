package io.overcoded.vaadin.grid;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import jakarta.persistence.EntityManager;

import java.util.List;

@SpringComponent
public class GridFilterRepositoryFactory {
    public <T> GridFilterRepository<T> create(EntityManager entityManager, List<AbstractField<?, ?>> filterComponents, Class<T> type) {
        return new GridFilterRepository<>(filterComponents, entityManager, type);
    }
}
