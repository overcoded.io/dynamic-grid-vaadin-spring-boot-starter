package io.overcoded.vaadin.grid;

/**
 * Marker interface to be able to find parent view, where attached methods can be triggered.
 */
public interface HasDynamicDialog {
}
