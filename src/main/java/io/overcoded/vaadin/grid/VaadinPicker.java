package io.overcoded.vaadin.grid;

import com.vaadin.flow.component.customfield.CustomField;
import io.overcoded.grid.Picker;

public abstract class VaadinPicker<T> extends CustomField<T> implements Picker<T> {
}
