package io.overcoded.vaadin.grid;

import io.overcoded.grid.PickerFactory;

public abstract class VaadinPickerFactory<T> implements PickerFactory<T> {
    public abstract VaadinPicker<T> get();
}
