package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.AbstractField;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

import java.util.Objects;

public abstract class AbstractEditorComponentSupplier<C extends AbstractField<C, T>, T> implements EditorComponentSupplier<C, T> {

    protected <X, Y extends T> void configureDialogFilter(C component, ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        if (isDialogFilter(columnInfo, dialogFilter)) {
            setDialogFilter(component, dialogFilter);
        }
    }

    protected <X, Y extends T> boolean isDialogFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        return Objects.nonNull(dialogFilter) && Objects.equals(columnInfo.getName(), dialogFilter.getProperty());
    }

    protected <X, Y extends T> void setDialogFilter(C component, DynamicDialogParameter<X, Y> dialogFilter) {
        component.setValue(dialogFilter.getParameter());
        component.setReadOnly(true);
    }
}
