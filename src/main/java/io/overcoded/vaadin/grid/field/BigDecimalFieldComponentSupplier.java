package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

import java.math.BigDecimal;

@SpringComponent
public class BigDecimalFieldComponentSupplier extends AbstractEditorComponentSupplier<BigDecimalField, BigDecimal> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.DEFAULT;
    }

    @Override
    public Class<BigDecimal> getValueType() {
        return BigDecimal.class;
    }

    @Override
    public <X, Y extends BigDecimal> BigDecimalField create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        BigDecimalField component = new BigDecimalField(columnInfo.getLabel());
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends BigDecimal> BigDecimalField createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        BigDecimalField component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }
}
