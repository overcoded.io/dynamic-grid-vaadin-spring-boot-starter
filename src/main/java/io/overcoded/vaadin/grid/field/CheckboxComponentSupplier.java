package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

@SpringComponent
public class CheckboxComponentSupplier extends AbstractEditorComponentSupplier<Checkbox, Boolean> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.DEFAULT;
    }

    @Override
    public Class<Boolean> getValueType() {
        return Boolean.class;
    }

    @Override
    public <X, Y extends Boolean> Checkbox create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        Checkbox component = new Checkbox(columnInfo.getLabel());
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends Boolean> Checkbox createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        Checkbox component = create(columnInfo);
        component.setId(columnInfo.getName());
        return component;
    }
}
