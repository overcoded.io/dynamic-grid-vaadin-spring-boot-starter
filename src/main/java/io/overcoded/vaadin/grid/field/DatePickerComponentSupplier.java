package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

import java.time.LocalDate;

@SpringComponent
public class DatePickerComponentSupplier extends AbstractEditorComponentSupplier<DatePicker, LocalDate> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.DATE_PICKER;
    }

    @Override
    public Class<LocalDate> getValueType() {
        return LocalDate.class;
    }

    @Override
    public <X, Y extends LocalDate> DatePicker create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        DatePicker component = new DatePicker(columnInfo.getLabel());
        component.setI18n(new DatePicker.DatePickerI18n().setDateFormat("yyyy-MM-dd"));
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends LocalDate> DatePicker createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        DatePicker component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }
}
