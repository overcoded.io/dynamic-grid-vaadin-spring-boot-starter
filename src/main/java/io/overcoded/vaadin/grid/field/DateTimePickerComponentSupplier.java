package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

import java.time.LocalDateTime;

@SpringComponent
public class DateTimePickerComponentSupplier extends AbstractEditorComponentSupplier<DateTimePicker, LocalDateTime> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.DATE_TIME_PICKER;
    }

    @Override
    public Class<LocalDateTime> getValueType() {
        return LocalDateTime.class;
    }

    @Override
    public <X, Y extends LocalDateTime> DateTimePicker create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        DateTimePicker component = new DateTimePicker(columnInfo.getLabel());
        component.setDatePickerI18n(new DatePicker.DatePickerI18n().setDateFormat("yyyy-MM-dd"));
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends LocalDateTime> DateTimePicker createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        DateTimePicker component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setDatePlaceholder("filter by " + columnInfo.getLabel().toLowerCase());
        return component;
    }
}
