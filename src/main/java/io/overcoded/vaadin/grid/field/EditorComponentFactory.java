package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.GridInfo;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringComponent
@RequiredArgsConstructor
public class EditorComponentFactory {
    private final List<EditorComponentSupplier> editorComponentFactories;

    public <T, U> List<AbstractField<?, ?>> createFilterComponents(GridInfo gridInfo, DynamicDialogParameter<T, U> dialogFilter) {
        return new ArrayList<>(gridInfo.getColumns().stream()
                .filter(ColumnInfo::isFilter)
                .map((ColumnInfo info) -> createFilterComponent(info, dialogFilter))
                .toList());
    }

    public <T, U> AbstractField<?, ?> createFormComponent(ColumnInfo info, DynamicDialogParameter<T, U> dialogFilter) {
        return create(info, dialogFilter)
                .map(factory -> factory.create(info, dialogFilter))
                .orElseGet(() -> new TextField(info.getLabel()));
    }

    public <T, U> AbstractField<?, ?> createFilterComponent(ColumnInfo info, DynamicDialogParameter<T, U> dialogFilter) {
        return create(info, dialogFilter)
                .map(factory -> factory.createFilter(info, dialogFilter))
                .orElseGet(() -> new TextField());
    }

    private <T, U> Optional<EditorComponentSupplier> create(ColumnInfo info, DynamicDialogParameter<T, U> dialogFilter) {
        return getEditorComponentSupplierCandidateStream(info)
                .findFirst();
    }

    private Stream<EditorComponentSupplier> getEditorComponentSupplierCandidateStream(ColumnInfo info) {
        List<EditorComponentSupplier> componentSuppliers = getMatchingEditorComponentSuppliers(info);
        Stream<EditorComponentSupplier> stream = componentSuppliers.stream();
        if (componentSuppliers.size() > 1) {
            stream = stream.filter(factory -> info.getType().isAssignableFrom(factory.getValueType()));
        }
        return stream;
    }

    private List<EditorComponentSupplier> getMatchingEditorComponentSuppliers(ColumnInfo info) {
        return editorComponentFactories.stream()
                .filter(factory -> factory.getType() == info.getFieldProviderType())
                .collect(Collectors.toList());
    }
}
