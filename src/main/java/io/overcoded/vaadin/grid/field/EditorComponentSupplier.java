package io.overcoded.vaadin.grid.field;


import com.vaadin.flow.component.AbstractField;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

public interface EditorComponentSupplier<C extends AbstractField<C, T>, T> {
    FieldProviderType getType();

    Class<T> getValueType();

    default C create(ColumnInfo columnInfo) {
        return create(columnInfo, null);
    }

    <X, Y extends T> C create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter);

    <X, Y extends T> C createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter);

}
