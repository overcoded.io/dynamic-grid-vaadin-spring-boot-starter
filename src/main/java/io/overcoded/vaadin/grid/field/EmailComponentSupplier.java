package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

@SpringComponent
public class EmailComponentSupplier extends AbstractEditorComponentSupplier<EmailField, String> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.EMAIL;
    }

    @Override
    public Class<String> getValueType() {
        return String.class;
    }

    @Override
    public <X, Y extends String> EmailField create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        EmailField component = new EmailField(columnInfo.getLabel());
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends String> EmailField createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        EmailField component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }
}
