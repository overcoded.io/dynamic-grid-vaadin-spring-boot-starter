package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

@SpringComponent
public class EnumComponentSupplier extends AbstractEditorComponentSupplier<ComboBox<Enum>, Enum> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.ENUM;
    }

    @Override
    public Class<Enum> getValueType() {
        return Enum.class;
    }


    @Override
    public <X, Y extends Enum> ComboBox<Enum> create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        Class<Y> type = (Class<Y>) columnInfo.getType();
        ComboBox<Y> component = new ComboBox<>(columnInfo.getLabel(), type.getEnumConstants());
        configureDialogFilter((ComboBox<Enum>) component, columnInfo, dialogFilter);
        return (ComboBox<Enum>) component;
    }

    @Override
    public <X, Y extends Enum> ComboBox<Enum> createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        ComboBox<Enum> component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }
}
