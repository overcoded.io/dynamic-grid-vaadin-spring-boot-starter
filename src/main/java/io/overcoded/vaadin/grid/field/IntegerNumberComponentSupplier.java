package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

@SpringComponent
public class IntegerNumberComponentSupplier extends AbstractEditorComponentSupplier<IntegerField, Integer> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.DEFAULT;
    }

    @Override
    public Class<Integer> getValueType() {
        return Integer.class;
    }

    @Override
    public <X, Y extends Integer> IntegerField create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        IntegerField component = new IntegerField(columnInfo.getLabel());
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends Integer> IntegerField createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        IntegerField component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }
}
