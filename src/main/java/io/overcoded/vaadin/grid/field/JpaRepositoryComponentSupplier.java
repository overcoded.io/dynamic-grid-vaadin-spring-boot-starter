package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;
import io.overcoded.vaadin.grid.ComboBoxCallbackFactory;
import io.overcoded.vaadin.grid.ComboBoxFilterRepository;
import io.overcoded.vaadin.grid.ComboBoxFilterRepositoryFactory;
import io.overcoded.vaadin.grid.DynamicItemLabelGeneratorFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.support.Repositories;

import java.util.Objects;
import java.util.Optional;

@SpringComponent
@RequiredArgsConstructor
public class JpaRepositoryComponentSupplier extends AbstractEditorComponentSupplier<ComboBox<Object>, Object> {
    private final Repositories repositories;
    private final ComboBoxCallbackFactory comboBoxCallbackFactory;
    private final DynamicItemLabelGeneratorFactory itemLabelGeneratorFactory;
    private final ComboBoxFilterRepositoryFactory comboBoxFilterRepositoryFactory;

    @Override
    public FieldProviderType getType() {
        return FieldProviderType.JPA_REPOSITORY;
    }

    @Override
    public Class<Object> getValueType() {
        return Object.class;
    }

    @Override
    public <X, Y> ComboBox<Object> create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        JpaRepository<?, ?> repository = (JpaRepository<?, ?>) repositories.getRepositoryFor(columnInfo.getType()).orElse(null);
        ComboBox<Object> component = new ComboBox<>(columnInfo.getLabel());
        if (isDialogFilter(columnInfo, dialogFilter)) {
            component.setItems(dialogFilter.getParameter());
            setDialogFilter(component, dialogFilter);
        } else if (Objects.nonNull(repository)) {
            configureFetchCallback(columnInfo.getType(), component, repository);
        }
        if (!columnInfo.getDisplayValueExpressionParts().isEmpty()) {
            component.setItemLabelGenerator(itemLabelGeneratorFactory.create(columnInfo.getDisplayValueExpressionParts()));
        }
        return component;
    }

    @Override
    public <X, Y> ComboBox<Object> createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        ComboBox<Object> component = create(columnInfo, dialogFilter);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }

    private <X> void configureFetchCallback(Class<?> type, ComboBox<Object> comboBox, JpaRepository<?, ?> jpaRepository) {
        Optional<ComboBoxFilterRepository<X>> possibleFilterRepository = comboBoxFilterRepositoryFactory.create((Class<X>) type);
        if (possibleFilterRepository.isPresent()) {
            ComboBoxFilterRepository<X> filterRepository = possibleFilterRepository.get();
            comboBox.setItems(comboBoxCallbackFactory.getFetchItemsCallback(filterRepository, (JpaRepository<X, Long>) jpaRepository));
        } else {
            comboBox.setItems(comboBoxCallbackFactory.getFetchItemsCallback((JpaRepository<X, Long>) jpaRepository));
        }
    }
}
