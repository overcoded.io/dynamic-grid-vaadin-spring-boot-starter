package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.component.textfield.LongField;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

@SpringComponent
public class LongNumberComponentSupplier extends AbstractEditorComponentSupplier<LongField, Long> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.DEFAULT;
    }

    @Override
    public Class<Long> getValueType() {
        return Long.class;
    }

    @Override
    public <X, Y extends Long> LongField create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        LongField component = new LongField(columnInfo.getLabel());
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends Long> LongField createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        LongField component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }
}
