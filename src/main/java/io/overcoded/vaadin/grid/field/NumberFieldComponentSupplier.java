package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

@SpringComponent
public class NumberFieldComponentSupplier extends AbstractEditorComponentSupplier<NumberField, Double> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.DEFAULT;
    }

    @Override
    public Class<Double> getValueType() {
        return Double.class;
    }

    @Override
    public <X, Y extends Double> NumberField create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        NumberField component = new NumberField(columnInfo.getLabel());
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends Double> NumberField createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        NumberField component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }
}
