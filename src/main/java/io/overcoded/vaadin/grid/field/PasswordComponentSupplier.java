package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

@SpringComponent
public class PasswordComponentSupplier extends AbstractEditorComponentSupplier<PasswordField, String> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.PASSWORD;
    }

    @Override
    public Class<String> getValueType() {
        return String.class;
    }

    @Override
    public <X, Y extends String> PasswordField create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        PasswordField component = new PasswordField(columnInfo.getLabel());
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends String> PasswordField createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        PasswordField component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }
}
