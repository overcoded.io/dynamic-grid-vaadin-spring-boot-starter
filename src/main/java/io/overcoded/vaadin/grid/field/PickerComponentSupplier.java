package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.shared.HasClearButton;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;
import io.overcoded.vaadin.grid.VaadinPicker;
import io.overcoded.vaadin.grid.VaadinPickerFactory;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Objects;

@SpringComponent
@RequiredArgsConstructor
public class PickerComponentSupplier extends AbstractEditorComponentSupplier<CustomField<Object>, Object> {
    private final List<VaadinPickerFactory<?>> pickers;

    @Override
    public FieldProviderType getType() {
        return FieldProviderType.PICKER;
    }

    @Override
    public Class<Object> getValueType() {
        return Object.class;
    }

    @Override
    public <X, Y> VaadinPicker<Object> create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        VaadinPicker<Y> vaadinPicker = (VaadinPicker<Y>) pickers.stream()
                .filter(picker -> Objects.equals(columnInfo.getType(), picker.getType()))
                .findFirst()
                .orElseGet(FallbackVaadinPickerFactory::new)
                .get();
        if (Objects.nonNull(vaadinPicker)) {
            vaadinPicker.setLabel(columnInfo.getLabel());
        }
        return (VaadinPicker<Object>) vaadinPicker;
    }

    @Override
    public <X, Y> VaadinPicker<Object> createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        CustomField<Y> component = (CustomField<Y>) create(columnInfo);
        if (Objects.nonNull(component)) {
            component.setLabel(null);
            component.setId(columnInfo.getName());
            if (component instanceof HasClearButton hasClearButton) {
                hasClearButton.setClearButtonVisible(true);
            }
            if (columnInfo.getName().equals(dialogFilter.getProperty())) {
                component.setValue(dialogFilter.getParameter());
                component.setReadOnly(true);
            }
        }
        return (VaadinPicker<Object>) component;
    }

    private static class FallbackVaadinPickerFactory extends VaadinPickerFactory<Object> {
        @Override
        public VaadinPicker<Object> get() {
            return null;
        }

        @Override
        public Class<Object> getType() {
            return Object.class;
        }
    }
}
