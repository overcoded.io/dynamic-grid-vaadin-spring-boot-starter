
package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

@SpringComponent
public class TextAreaComponentSupplier extends AbstractEditorComponentSupplier<TextArea, String> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.TEXT_AREA;
    }

    @Override
    public Class<String> getValueType() {
        return String.class;
    }

    @Override
    public <X, Y extends String> TextArea create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        TextArea component = new TextArea(columnInfo.getLabel());
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends String> TextArea createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        TextArea component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }
}
