package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

@SpringComponent
public class TextFieldComponentSupplier extends AbstractEditorComponentSupplier<TextField, String> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.DEFAULT;
    }

    @Override
    public Class<String> getValueType() {
        return String.class;
    }

    @Override
    public <X, Y extends String> TextField create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        TextField component = new TextField(columnInfo.getLabel());
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends String> TextField createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        TextField component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }
}
