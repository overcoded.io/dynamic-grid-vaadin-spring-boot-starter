package io.overcoded.vaadin.grid.field;

import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.ColumnInfo;
import io.overcoded.grid.annotation.FieldProviderType;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;

import java.time.LocalTime;

@SpringComponent
public class TimePickerComponentSupplier extends AbstractEditorComponentSupplier<TimePicker, LocalTime> {
    @Override
    public FieldProviderType getType() {
        return FieldProviderType.TIME_PICKER;
    }

    @Override
    public Class<LocalTime> getValueType() {
        return LocalTime.class;
    }

    @Override
    public <X, Y extends LocalTime> TimePicker create(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        TimePicker component = new TimePicker(columnInfo.getLabel());
        configureDialogFilter(component, columnInfo, dialogFilter);
        return component;
    }

    @Override
    public <X, Y extends LocalTime> TimePicker createFilter(ColumnInfo columnInfo, DynamicDialogParameter<X, Y> dialogFilter) {
        TimePicker component = create(columnInfo);
        component.setLabel(null);
        component.setId(columnInfo.getName());
        component.setClearButtonVisible(true);
        component.setPlaceholder("filter by " + columnInfo.getName());
        return component;
    }
}
