package io.overcoded.vaadin.grid.menu;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.LinkGridMenuItem;
import io.overcoded.grid.annotation.GridAnchorTarget;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class BlankTargetExecutor implements GridAnchorTargetExecutor {
    private final ExpressionParser expressionParser;

    @Override
    public GridAnchorTarget getType() {
        return GridAnchorTarget.BLANK;
    }

    @Override
    public void execute(UI ui, LinkGridMenuItem menuItem, Object parent, Component sourceView) {
        List<String> arguments = getArguments(parent, menuItem);
        execute(ui, menuItem.getLink(), arguments);
    }

    protected void execute(UI ui, String url, List<String> args) {
        Object[] arguments = args.toArray(new String[]{});
        String finalUrl = String.format(url, arguments);
        if (Objects.nonNull(ui) && Objects.nonNull(ui.getPage())) {
            ui.getPage().open(finalUrl, "_blank");
        } else {
            log.error("Failed to open {}, seems ui (or page) is null", finalUrl);
        }
    }

    protected <U> List<String> getArguments(U item, LinkGridMenuItem gridMenuItem) {
        List<String> result = List.of();
        try {
            EvaluationContext context = new StandardEvaluationContext(item);
            result = gridMenuItem.getArguments().stream()
                    .map(expressionParser::parseExpression)
                    .map(expression -> expression.getValue(context))
                    .map(String::valueOf)
                    .collect(Collectors.toList());
        } catch (EvaluationException ex) {
            log.trace("SpelExpression apply failed. {}", ex.getMessage());
        }
        return result;
    }
}
