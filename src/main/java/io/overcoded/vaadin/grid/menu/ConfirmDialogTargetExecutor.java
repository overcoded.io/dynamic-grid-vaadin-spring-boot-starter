package io.overcoded.vaadin.grid.menu;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.LinkGridMenuItem;
import io.overcoded.grid.annotation.GridAnchorTarget;
import io.overcoded.vaadin.dialog.DynamicConfirmDialog;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class ConfirmDialogTargetExecutor implements GridAnchorTargetExecutor {
    protected final ExpressionParser expressionParser;
    @Override
    public GridAnchorTarget getType() {
        return GridAnchorTarget.CONFIRM_DIALOG;
    }

    @Override
    public void execute(UI ui, LinkGridMenuItem menuItem, Object parent, Component sourceView) {
        Optional<Method> instanceMethod = getMethod(sourceView, parent, menuItem.getMethod());
        if (instanceMethod.isPresent()) {
            instanceMethod
                    .map(method -> invokeMethodRunnable(method, sourceView, menuItem, parent))
                    .map(callback -> new DynamicConfirmDialog(menuItem.getLabel(), menuItem.getConfiguration(), callback))
                    .ifPresentOrElse(ConfirmDialog::open, () -> log.error("Confirm dialog can't be calculated!"));
        } else {
            new DynamicConfirmDialog(menuItem.getLabel(), menuItem.getConfiguration(), invokeSpelMethod(menuItem.getMethod(), parent));
        }
    }

    protected Runnable invokeSpelMethod(String method, Object parent) {
        StandardEvaluationContext context = new StandardEvaluationContext(parent);
        return () -> expressionParser.parseExpression(method).getValue(context);
    }

    private Runnable invokeMethodRunnable(Method method, Component source, LinkGridMenuItem menuItem, Object parent) {
        return () -> {
            try {
                method.setAccessible(true);
                method.invoke(source, parent, menuItem);
                method.setAccessible(false);
            } catch (IllegalAccessException | InvocationTargetException e) {
                log.error("Failed to execute " + method.getName() + ": {}", e.getMessage());
            }
        };
    }

    protected Optional<Method> getMethod(Component source, Object parent, String methodName) {
        Optional<Method> result = Optional.empty();
        if (Objects.nonNull(methodName) && !"DEFAULT".equals(methodName)) {
            Class<?> parentType = parent.getClass();
            try {
                result = Optional.of(source.getClass().getDeclaredMethod(methodName, parentType, LinkGridMenuItem.class));
            } catch (Exception e) {
                log.warn("Failed to found instance method, falling back to SPEL evaluation! {}", methodName);
            }
        }
        return result;
    }
}
