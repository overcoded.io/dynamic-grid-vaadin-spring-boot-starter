package io.overcoded.vaadin.grid.menu;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.DialogGridMenuItem;
import io.overcoded.grid.GridInfo;
import io.overcoded.grid.processor.FieldCollector;
import io.overcoded.vaadin.dialog.DynamicDialogFactory;
import io.overcoded.vaadin.dialog.DynamicDialogParameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.function.BiConsumer;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class DialogGridMenuItemClickListener implements GridMenuItemClickListener<DialogGridMenuItem> {
    private final DynamicDialogFactory dynamicDialogFactory;
    private final ExpressionParser expressionParser;
    private final FieldCollector fieldCollector;

    @Override
    public Class<DialogGridMenuItem> getType() {
        return DialogGridMenuItem.class;
    }

    @Override
    public <T, U> void trigger(GridCrud<U> gridCrud, U item, DialogGridMenuItem dialogGridMenuItem, UI ui, BiConsumer<GridCrud<T>, GridInfo> configurer) {
        DynamicDialogParameter<T, U> parameter = getDialogParameter(item, dialogGridMenuItem);
        Dialog dialog = dynamicDialogFactory.create(gridCrud, parameter, configurer);
        dialog.open();
    }

    @SuppressWarnings("unchecked")
    private <T, U, Z> DynamicDialogParameter<T, U> getDialogParameter(Z item, DialogGridMenuItem entry) {
        Class<T> type = (Class<T>) entry.getType();
        U parameter = getParameter(entry, item);
        String property = getProperty(entry);
        Class<U> parameterType = (Class<U>) parameter.getClass();
        String reverseProperty = getReverseProperty(parameterType, type);
        return DynamicDialogParameter.<T, U>builder()
                .ownedEntries(getOwnedItems(parameter, reverseProperty))
                .reverseProperty(reverseProperty)
                .parameterType(parameterType)
                .parameter(parameter)
                .property(property)
                .entry(entry)
                .type(type)
                .build();
    }

    private String getProperty(DialogGridMenuItem entry) {
        return entry.isCustomFilter() ? entry.getParentFieldName() : entry.getName();
    }

    private <T, Z> T getParameter(DialogGridMenuItem entry, Z item) {
        return entry.isCustomFilter() ? getField(item, entry.getParentFieldName()) : (T) item;
    }

    private <T, Z> T getField(Z item, String property) {
        T result = (T) item;
        try {
            Expression expression = expressionParser.parseExpression(property);
            EvaluationContext context = new StandardEvaluationContext(item);
            result = (T) expression.getValue(context);
        } catch (EvaluationException ex) {
            log.trace("SpelExpression apply failed. {}", ex.getMessage());
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private <U, T> List<T> getOwnedItems(U parameter, String reverseProperty) {
        List<T> result = new ArrayList<>();
        try {
            Expression expression = expressionParser.parseExpression(reverseProperty);
            EvaluationContext context = new StandardEvaluationContext(parameter);
            Object value = expression.getValue(context);
            if (Objects.nonNull(value)) {
                if (Collection.class.isAssignableFrom(value.getClass())) {
                    result.addAll((Collection<T>) value);
                } else {
                    result.add((T) value);
                }
            }
        } catch (EvaluationException ex) {
            log.trace("SpelExpression apply failed. {}", ex.getMessage());
        }
        return result;
    }

    private <U, T> String getReverseProperty(Class<U> parameterType, Class<T> type) {
        return fieldCollector.getFields(parameterType)
                .stream()
                .filter(field -> isMatchingField(field, type))
                .map(Field::getName)
                .findFirst()
                .orElse(null);
    }

    private boolean isMatchingField(Field field, Class<?> expectedType) {
        boolean result = field.getType().equals(expectedType);
        if (field.getType().isAssignableFrom(Collection.class)
                || field.getType().isAssignableFrom(Set.class)
                || field.getType().isAssignableFrom(List.class)) {
            Class<?> actualType = getGenericType(field);
            result = expectedType.equals(actualType);
        }
        return result;
    }

    private Class<?> getGenericType(Field field) {
        ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        return (Class<?>) actualTypeArguments[0];
    }

}
