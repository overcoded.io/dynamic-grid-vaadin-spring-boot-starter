package io.overcoded.vaadin.grid.menu;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.LinkGridMenuItem;
import io.overcoded.grid.annotation.GridAnchorTarget;
import lombok.extern.slf4j.Slf4j;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.function.Supplier;

@Slf4j
@SpringComponent
public class DialogTargetExecutor extends ConfirmDialogTargetExecutor implements GridAnchorTargetExecutor {
    public DialogTargetExecutor(ExpressionParser expressionParser) {
        super(expressionParser);
    }

    @Override
    public GridAnchorTarget getType() {
        return GridAnchorTarget.DIALOG;
    }

    @Override
    public void execute(UI ui, LinkGridMenuItem menuItem, Object parent, Component sourceView) {
        Optional<Method> instanceMethod = getMethod(sourceView, parent, menuItem.getMethod());
        if (instanceMethod.isPresent()) {
            instanceMethod
                    .map(method -> generateSupplierMethod(method, sourceView, menuItem, parent))
                    .map(Supplier::get)
                    .ifPresentOrElse(Dialog::open, () -> log.error("Custom dialog can't be opened!"));
        } else {
            generateSpelDialog(menuItem.getMethod(), parent).open();
        }
    }

    protected Dialog generateSpelDialog(String method, Object parent) {
        StandardEvaluationContext context = new StandardEvaluationContext(parent);
        return expressionParser.parseExpression(method).getValue(context, Dialog.class);
    }

    private Supplier<Dialog> generateSupplierMethod(Method method, Component source, LinkGridMenuItem menuItem, Object parent) {
        return () -> {
            Dialog result = null;
            try {
                method.setAccessible(true);
                result = (Dialog) method.invoke(source, parent, menuItem);
                method.setAccessible(false);
            } catch (IllegalAccessException | InvocationTargetException e) {
                log.error("Failed to execute " + method.getName() + ": {}", e.getMessage());
            }
            return result;
        };
    }
}
