package io.overcoded.vaadin.grid.menu;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import io.overcoded.grid.LinkGridMenuItem;
import io.overcoded.grid.annotation.GridAnchorTarget;

public interface GridAnchorTargetExecutor {
    GridAnchorTarget getType();

    void execute(UI ui, LinkGridMenuItem menuItem, Object parent, Component sourceView);
}
