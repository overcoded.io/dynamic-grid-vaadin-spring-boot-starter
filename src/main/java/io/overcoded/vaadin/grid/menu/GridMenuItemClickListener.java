package io.overcoded.vaadin.grid.menu;

import com.vaadin.flow.component.UI;
import io.overcoded.grid.GridInfo;
import io.overcoded.grid.GridMenuItem;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.util.function.BiConsumer;

public interface GridMenuItemClickListener<I extends GridMenuItem> {
    Class<I> getType();

    <T, U> void trigger(GridCrud<U> gridCrud, U item, I dialogGridMenuItem, UI ui, BiConsumer<GridCrud<T>, GridInfo> configurer);
}
