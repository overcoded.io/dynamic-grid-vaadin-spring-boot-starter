package io.overcoded.vaadin.grid.menu;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.GridInfo;
import io.overcoded.grid.GridMenuItem;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class GridMenuItemClickListenerFactory {
    private final List<GridMenuItemClickListener<?>> listeners;

    public <T, U, I extends GridMenuItem> void trigger(GridCrud<U> gridCrud, U item, I gridMenuItem, UI ui, BiConsumer<GridCrud<T>, GridInfo> configurer) {
        Optional<GridMenuItemClickListener<?>> clickListener = listeners.stream()
                .filter(listener -> listener.getType().equals(gridMenuItem.getClass()))
                .findFirst();
        if (clickListener.isPresent()) {
            GridMenuItemClickListener<I> gridMenuItemClickListener = (GridMenuItemClickListener<I>) clickListener.get();
            gridMenuItemClickListener.trigger(gridCrud, item, gridMenuItem, ui, configurer);
        } else {
            log.info("No-op click, on item: {}", item);
        }
    }
}
