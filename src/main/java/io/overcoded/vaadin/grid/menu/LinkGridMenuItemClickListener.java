package io.overcoded.vaadin.grid.menu;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.GridInfo;
import io.overcoded.grid.LinkGridMenuItem;
import io.overcoded.vaadin.grid.HasDynamicDialog;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

@Slf4j
@SpringComponent
@RequiredArgsConstructor
public class LinkGridMenuItemClickListener implements GridMenuItemClickListener<LinkGridMenuItem> {
    private final List<GridAnchorTargetExecutor> targetExecutors;

    @Override
    public Class<LinkGridMenuItem> getType() {
        return LinkGridMenuItem.class;
    }

    @Override
    public <T, U> void trigger(GridCrud<U> gridCrud, U item, LinkGridMenuItem gridMenuItem, UI ui, BiConsumer<GridCrud<T>, GridInfo> configurer) {
        targetExecutors.stream()
                .filter(executor -> executor.getType() == gridMenuItem.getTarget())
                .findFirst()
                .ifPresent(executor -> executor.execute(ui, gridMenuItem, item, sourceView(gridCrud)));
    }

    private <U> Component sourceView(GridCrud<U> gridCrud) {
        Component source = null;
        Optional<Component> parent = gridCrud.getParent();
        while (parent.isPresent() && !(parent.get() instanceof HasDynamicDialog)) {
            parent = parent.flatMap(Component::getParent);
        }
        if (parent.isPresent()) {
            source = parent.get();
        }
        return source;
    }
}
