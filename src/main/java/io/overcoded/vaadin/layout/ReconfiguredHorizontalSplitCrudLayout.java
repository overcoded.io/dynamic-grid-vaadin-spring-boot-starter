package io.overcoded.vaadin.layout;

import com.vaadin.flow.component.splitlayout.SplitLayout;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.crudui.layout.impl.HorizontalSplitCrudLayout;

@Slf4j
public class ReconfiguredHorizontalSplitCrudLayout extends HorizontalSplitCrudLayout {
    @Override
    protected SplitLayout buildMainLayout() {
        SplitLayout splitLayout = super.buildMainLayout();
        splitLayout.setSplitterPosition(76.0);
        return splitLayout;
    }
}
