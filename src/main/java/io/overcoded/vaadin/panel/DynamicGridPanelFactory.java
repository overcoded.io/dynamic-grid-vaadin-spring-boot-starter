package io.overcoded.vaadin.panel;

import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
public class DynamicGridPanelFactory extends PanelFactory {
    @Deprecated
    public Panel createInfoPanel(String title, String message) {
        return createPrimary(title, new Paragraph(message));
    }

    @Deprecated
    public Panel createErrorPanel(String title, String message) {
        return createError(title, new Paragraph(message));
    }

    @Deprecated
    public Panel createSuccessPanel(String title, String message) {
        return createSuccess(title, new Paragraph(message));
    }
}
