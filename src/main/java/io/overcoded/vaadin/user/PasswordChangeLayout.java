package io.overcoded.vaadin.user;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import io.overcoded.grid.security.GridUser;
import lombok.extern.slf4j.Slf4j;

import java.util.function.BiConsumer;

@Slf4j
public class PasswordChangeLayout extends VerticalLayout {
    public PasswordChangeLayout(H2 title, GridUser gridUser, BiConsumer<String, String> callback) {
        EmailField emailField = new EmailField("Email");
        emailField.setValue(gridUser.getEmail());
        emailField.setReadOnly(true);
        TextField usernameField = new TextField("Username");
        usernameField.setValue(gridUser.getUsername());
        usernameField.setReadOnly(true);
        PasswordField oldPassword = new PasswordField("Old password");
        PasswordField newPassword = new PasswordField("New password");
        PasswordField confirmPassword = new PasswordField("Confirm password");

        oldPassword.setRequiredIndicatorVisible(true);
        newPassword.setRequiredIndicatorVisible(true);
        confirmPassword.setRequiredIndicatorVisible(true);

        FormLayout formLayout = new FormLayout();
        formLayout.add(emailField, usernameField, oldPassword, newPassword, confirmPassword);
        formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 2));
        formLayout.setColspan(emailField, 2);

        Button cancel = new Button("Cancel");
        cancel.addClickListener(event -> {
            newPassword.clear();
            confirmPassword.clear();
        });

        Button changePassword = new Button("Change password");
        changePassword.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        changePassword.addClickListener(event -> {
            if (newPassword.getValue().equals(confirmPassword.getValue())) {
                try {
                    callback.accept(oldPassword.getValue(), newPassword.getValue());
                } catch (IllegalArgumentException ex) {
                    newPassword.setHelperText(ex.getMessage());
                }
            } else {
                log.info("Passwords are not matching!");
                newPassword.setHelperText("Confirmation password is not matching with the new password!");
            }
        });

        HorizontalLayout buttonLayout = new HorizontalLayout(cancel, changePassword);
        buttonLayout.getStyle().set("flex-wrap", "wrap");
        buttonLayout.setJustifyContentMode(JustifyContentMode.END);

        add(title, formLayout, buttonLayout);
    }
}
