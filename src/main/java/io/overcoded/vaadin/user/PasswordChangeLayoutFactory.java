package io.overcoded.vaadin.user;

import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.GridSecurityConfigurationProperties;
import io.overcoded.grid.security.GridSecurityService;
import io.overcoded.grid.security.GridUserDetailsManager;
import lombok.RequiredArgsConstructor;

@SpringComponent
@RequiredArgsConstructor
public class PasswordChangeLayoutFactory {
    private final GridUserDetailsManager gridUserDetailsManager;
    private final GridSecurityService securityService;
    private final GridSecurityConfigurationProperties securityProperties;

    public PasswordChangeLayout create() {
        H2 title = createTitle();
        return new PasswordChangeLayout(title, securityService.getAuthenticatedUser(), (oldValue, newValue) -> {
            gridUserDetailsManager.changePassword(oldValue, newValue);
            securityService.logout();
        });
    }

    private H2 createTitle() {
        H2 title = new H2(securityProperties.getChangePasswordTitle());
        title.getElement().getStyle().set("margin", "0rem");
        return title;
    }
}
