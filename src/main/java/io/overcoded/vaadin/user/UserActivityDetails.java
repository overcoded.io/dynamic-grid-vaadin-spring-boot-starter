package io.overcoded.vaadin.user;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.details.Details;
import io.overcoded.grid.GridSecurityConfigurationProperties;
import io.overcoded.grid.security.GridUserActivity;
import io.overcoded.grid.util.MarkdownParser;

import java.util.Objects;

public class UserActivityDetails extends Details {
    public UserActivityDetails(MarkdownParser markdownParser, GridSecurityConfigurationProperties properties, GridUserActivity<?> userActivity) {
        String timestamp = properties.getActivityTimestampFormatter().format(userActivity.getTimestamp());
        String actionMessageFormat = properties.getActionMessageFormat().getOrDefault(userActivity.getActionType(), properties.getUnknownActionMessageFormat());
        setSummaryText(actionMessageFormat.formatted(timestamp, userActivity.getUser().getUsername(), userActivity.getEntityType(), userActivity.getEntityId()));
        if (Objects.nonNull(userActivity.getDifference()) && !userActivity.getDifference().isBlank()) {
            setContent(new Html("<div>" + markdownParser.toHtml(userActivity.getDifference()) + "</div>"));
        }
    }
}
