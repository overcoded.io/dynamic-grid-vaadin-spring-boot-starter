package io.overcoded.vaadin.user;

import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.GridSecurityConfigurationProperties;
import io.overcoded.grid.security.GridUser;
import io.overcoded.grid.security.GridUserActivity;
import io.overcoded.grid.util.MarkdownParser;
import lombok.RequiredArgsConstructor;

@SpringComponent
@RequiredArgsConstructor
public class UserActivityDetailsFactory {
    private final MarkdownParser markdownParser;
    private final GridSecurityConfigurationProperties securityProperties;

    public <T extends GridUser> UserActivityDetails create(GridUserActivity<T> userActivity) {
        return new UserActivityDetails(markdownParser, securityProperties, userActivity);
    }
}
