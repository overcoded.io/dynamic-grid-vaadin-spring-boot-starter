package io.overcoded.vaadin.user;

import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.List;

public class UserActivityLayout extends VerticalLayout {

    public UserActivityLayout(H2 title, List<UserActivityDetails> pastActivities) {
        add(title);
        pastActivities.forEach(this::add);
    }
}
