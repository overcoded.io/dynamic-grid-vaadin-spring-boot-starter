package io.overcoded.vaadin.user;

import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.overcoded.grid.GridSecurityConfigurationProperties;
import io.overcoded.grid.security.GridUserActivityService;
import io.overcoded.vaadin.panel.DynamicGridPanelFactory;
import io.overcoded.vaadin.panel.Panel;
import lombok.RequiredArgsConstructor;

import java.util.List;

@SpringComponent
@RequiredArgsConstructor
public class UserActivityLayoutFactory {
    private final GridUserActivityService userActivityService;
    private final DynamicGridPanelFactory gridPanelFactory;
    private final UserActivityDetailsFactory userActivityDetailsFactory;
    private final GridSecurityConfigurationProperties securityProperties;

    public UserActivityLayout create() {
        H2 title = createTitle();
        List<UserActivityDetails> userActivityDetails = userActivityService.getPastActivities().stream().map(userActivityDetailsFactory::create).toList();
        UserActivityLayout layout = new UserActivityLayout(title, userActivityDetails);
        if (!securityProperties.isActivityTrackingEnabled()) {
            layout.addComponentAsFirst(createPanel(securityProperties.getActivityTitle(), securityProperties.getActivityTrackerDisabledMessage()));
        } else if (!securityProperties.isDifferenceTrackingEnabled()) {
            layout.addComponentAsFirst(createPanel(securityProperties.getDifferenceTrackerDisabledTitle(), securityProperties.getDifferenceTrackerDisabledMessage()));
        }
        layout.setSizeFull();
        return layout;
    }

    private H2 createTitle() {
        H2 title = new H2(securityProperties.getActivityTitle());
        title.getElement().getStyle().set("margin", "0rem");
        return title;
    }

    private Panel createPanel(String title, String message) {
        return gridPanelFactory.createPrimary(title, new Paragraph(message));
    }
}
