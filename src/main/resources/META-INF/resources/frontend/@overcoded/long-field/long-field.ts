
import { defineCustomElement } from '@vaadin/component-base/src/define.js';
import { NumberField } from '@vaadin/number-field/src/vaadin-number-field.js';

export class LongField extends NumberField {
    static get is() {
        return 'overcoded-long-field';
    }

    constructor() {
        super();
        this.allowedCharPattern = '[-+\\d]';
    }

    _valueChanged(newVal, oldVal) {
        if (newVal !== '' && !this.__isLong(newVal)) {
            console.warn(`Trying to set non-integer value "${newVal}" to <vaadin-integer-field>. Clearing the value.`);
            this.value = '';
            return;
        }
        super._valueChanged(newVal, oldVal);
    }

    _stepChanged(step, inputElement) {
        if (step != null && !this.__hasOnlyDigits(step)) {
            console.warn(
                `<overcoded-long-field> The \`step\` property must be a positive integer but \`${step}\` was provided, so the property was reset to \`null\`.`,
            );
            this.step = null;
            return;
        }

        super._stepChanged(step, inputElement);
    }

    __isLong(value) {
        return /^(-\d)?\d*$/u.test(String(value));
    }

    __hasOnlyDigits(value) {
        return /^\d+$/u.test(String(value));
    }
}

defineCustomElement(LongField);